## Infrastructure

- M: ~~Trouver la bonne chaîne de compilation pour le projet~~
- M: ~~Mettre à jour les Makefile et settings.json pour intégrer la cross-compilation~~

## Documentation

- A: ~~Créer le slide de présentation avec Google Slides et le partager avec les autres~~
- A: ~~Créer la partie problématique qui définit:~~
    - A: ~~L'architecture globale du projet (matériel et flux des données)~~
    - A: ~~Les différents capteurs et actionneurs~~
    - A: ~~La connectique du montage~~
    - A: ~~Les données (pour chaque capteur, quelle donnée, quel type, quelle plage)~~
    - A: ~~La problématique utilisateur à savoir: qu'est-ce qu'on veut faire avec les données~~
- A: ~~Créer la partie état de l'art qui définit:~~
    - A: ~~Les protocoles de transfert utilisés à savoir:~~
        - A: ~~I2C (Boussole, Température)~~
        - A: ~~UART (TX/RX Balise<->Master)~~
        - A: ~~TCP (Master<->Serveur)~~
    - A: ~~Les spécificités d'accès à chaque périphérique:~~
        - A: ~~BME280~~
        - A: ~~GY-271~~
        - A: ~~HC-SR501~~
        - A: ~~PCA9685~~
- M: ~~Créer la partie organisation et méthodologie qui décrit:~~
    - M: ~~L'organisation de l'équipe~~
    - M: ~~L'architecure générale du dépot~~
    - M: ~~L'organisation du travail~~
- I: ~~Créer la partie implémentation qui décrit:~~
    - I: ~~Le choix des librairies utilisées~~
    - I: ~~Le choix d'architecture~~
    - I: ~~Schéma de flux des données plus spécifiques (un schéma par application)~~
- M: ~~Créer la partie difficultés rencontrées~~
- M: ~~Créer la conclusion~~
- M: ~~Créer le slide remerciements~~

- I: ~~Créer la documentation du code:~~
    - I: ~~Commentaires doxygen pour les applications:~~
        - I: ~~balise~~
        - I: ~~master~~
        - M: ~~serveur~~
        - serveur-http
        - client
    - I: ~~Génération des classes et des schémas doxygen~~
    - I: ~~Diagramme des bases de données~~
    - I: ~~Diagramme de flux (doxygen?)~~

## Recherches

- ~~Protocole I2C avec :~~
    - A: ~~Configuration et accès GPIO normal~~
    - M: ~~Configuration et accès BME280~~
    - M: ~~Configuration et accès GY-271~~
    - M: ~~Configuration et accès Moteurs PCA9685~~
- ~~Protocole UART avec:~~
    - M: ~~protocole de transmission d'un message (configuration et méthodo)~~
    - M: ~~protocle de réception d'un message (configuration et méthodo)~~
- A, I: ~~Protocole TCP avec:~~
    - A, I: ~~Organisation client/serveur~~
    - A, I: ~~Protocole transmission/réception~~

## Developpement

- Application balise:
    - M: ~~Classe définissant un capteur avec méthodes init et getValues~~
    - I, M, A: ~~Création de capteurs spécifiques (dérivés de capteur):~~
        - M: ~~Capteur BME280: température, humidité, pression~~
        - A: ~~Capteur HC-SR501: présence détecté~~
        - M: ~~Capteur GY-271: x, y, z~~
    - M: ~~Classe déffinissant un transmitterTX avec méthodes init et send~~
    - M: ~~Application principale initialisant les capteurs et bouclant sur la récupération et transmission des données~~
    - I: ~~Ajouter un fichier de configuration de l'application (cycle de récuparation, adresses des capteurs)~~
    - M: ~~Ajouter un processus stopable de boucle infinie qui lit et transmet les données de chaque capteur~~
    - I: ~~Mettre en place le démarrage de l'application au démarrage de la raspberry (init.d?)~~
- Application master:
    - M: ~~Classe receiverRX avec méthodes init et receive~~
    - I: ~~Classe contrôleur permettant de contrôler les moteurs (implémenté mais les moteurs ne bougent pas)~~
    - M: ~~Classe messageHandler chargé de:~~
        - M: ~~Modifier l'état du drapeau jaune en fonction de la détection de présence~~
        - M: ~~Modifier l'état du drapeau bleu en fonction de l'état du système (réception, transmission, repos)~~
    - A: ~~Classe transmitterTCP avec méthod init et send~~
    - M: ~~Application gérant la boucle:~~
        - M: ~~Récupération des données:~~
        - M: ~~Actions sur les données(évaluation, transmission) et changement d'état du système (drapeaux)~~
    - M: ~~Ajouter un fichier de configuration de l'application (cycle de récuparation, adresses des capteurs)~~
    - I: ~~Mettre en place le démarrage de l'application au démarrage de la raspberry (init.d?)~~
- Application serveur:
    - M: ~~Classe receiverTCP qui reçoit les données~~
    - M: ~~Classe serialization qui enregistre les données en base de données~~
    - M: ~~Application http qui permet à l'utilisateur de:~~
        - M: ~~Renvoyer les dernières données d'un ou des capteurs~~
        - M: ~~Renvoyer un historique du dernier quart-d'heure pour les capteurs~~
        - M: ~~Renvoyer les statistiques (min, max, avg, count) de la dernière heure pour les capteurs~~
        - M: ~~Renvoyer un dashboard en html représentant les données, mise à jour en temps réel~~

 - Application serveur http:
    - M: ~~Classe de gestion des requêtes http~~
    - M: ~~Renvoyer des données, pour tous les capteurs ou un particulier:~~
        - M: ~~A l'instant t~~
        - M:~~Pour le dernier 1/4 d'heure~~
        - M:~~Des statisitiques sur la dernière heure~~
    - M: ~~ Créer un dashbooard de suivi de la balise~~

- Application client:
    - A, I: ~~Interface utilisateur permettant à l'aide de requêtes sur le serveur de:~~
        - A, I: ~~Choisir un capteur à observer à un instant t~~
        - A : ~~Afficher les statistiques pour un capteur donné~~
        - A: ~~Afficher l'historique pour un capteur donné~~
    - I: ~~Classe de communication avec le serveur~~
    - A: ~~Classe d'affichage~~
    - I: ~~Classe de configuration de requête~~


