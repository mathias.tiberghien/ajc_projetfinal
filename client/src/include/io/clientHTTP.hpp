#pragma once

#include <string>
#include <sstream>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include <iostream>
#include "json.hpp"
#include "log.hpp"

using namespace std;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using nljson = nlohmann::json_abi_v3_11_2::json;


/**
 * @brief Cette fonction envoie une requète http au serveur Web contenant l'ensemble
 * des données mesurées par la balise.
 * l'adresse http pointe sur des données formatées en json.
 * Celle-ci, une fois récupérées sont renvoyé dans un objet json de la lib nlohmann pour facilité d'utilisation
 * 
 * 
 * @param httpAdr adresse http qui contient la requète a envoyer au serveur
 * @return nljson objet json contenant les données retour de la requete
 */
nljson sendRequest(string httpAdr);
