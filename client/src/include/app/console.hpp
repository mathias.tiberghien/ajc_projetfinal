#ifndef CONSOLE_HPP_INCLUDED
#define CONSOLE_HPP_INCLUDED

#include "common.hpp"
#include "log.hpp"
#include <algorithm>
#include <string>
#include <cstring>
#include <cctype>
#include <cpprest/http_client.h>
#include <cpprest/json.h>
#include "clientHTTP.hpp"
#include "json.hpp"
#include <map>
#include <regex>

using nljson = nlohmann::json_abi_v3_11_2::json;
using namespace web;
using namespace web::http;
using namespace web::http::client;
using namespace std;

struct repSaisie {
    nljson data;
    string nmCapt;
    bool isPres;
    bool saveFile;
    string commande;
    time_t dateRef;
};
class console
{
    public:
        console();
        bool init(const nljson& config);
        void run();
    private:
        string getCapteur();
        string getStrSensor();
        string getStrHisto();
        string getStrStat();
        string getAdresse(string command);
        void afficheMenu();
        void selection_action();
        repSaisie saisie( string req, bool isStat = false);
        void quitter();
        void display_Stat(const nljson& value, string capteur);
        void Console_to_Sensor(repSaisie);
        void Console_to_Stats(repSaisie);
        void Console_to_StatsAll(repSaisie);
        void saveFile(repSaisie);
        void afficheInstantane();
        void afficheHistorique();
        void afficheStats();
        string getCapteurs();
        map<string,string> featuresMap;
        map<string,string> featuresUnits;
        vector<string> nomsCapteur;
        string endPoint;
        
};
#endif


