#include "console.hpp"



console::console()
{
    featuresMap["temperature"]="Température";
    featuresMap["humidite"]="Humidité";
    featuresMap["pression"]="Pression";
    featuresMap["x"]="X";
    featuresMap["y"]="Y";
    featuresMap["z"]="Z";
    featuresMap["presence"]="Présence";

    featuresUnits["temperature"]="°C";
    featuresUnits["humidite"]="%";
    featuresUnits["pression"]=" hPa";

    nomsCapteur = {"BME280", "GY-271", "HCSR501"};
}

string console::getCapteurs()
{
    stringstream s;
    s << "Choix parmi ";
    int i=0;
    int last_i = this->nomsCapteur.size()-1;
    for(string c : this->nomsCapteur)
    {
        s << c;
        s << (i < last_i ? (i < last_i -1 ? ", " : " et " ) : "");
        i++;
    }
    return s.str();
}

void console::run()
{
    cout << endl << "Bienvenue dans l'application qui vous permettra de récupérer les données des capteurs!" << endl << endl;
    cout << endl << "Vous pourrez récupérer ces données :" << endl;
    cout << " - en instantané (dernière valeur enregistrée)" << endl;
    cout << " - sous forme d'hitorique (dernier 1/4 d'heure enregistré)" << endl;
    cout << " - sous forme de statistiques (dernière heure enregistrée)" << endl;
    cout << " - Il est possible de spécifier une date de référence pour la recherche" << endl << endl;
    cout << "Bienvenue!!" << endl;
    cout << "Menu de l'appli:" << endl;


    afficheMenu();
    selection_action();
}

bool console::init(const nljson&config)
{
    stringstream s;
    string ip = config["ip"];
    int port = config["port"];
    s << "http://" << ip << ":" << port << "/api/data/";
    this->endPoint = s.str();
    return true;
}

void console::afficheMenu()
{

    cout << "-------------------------" << endl;
    cout << "(1) - Instantané" << endl;
    cout << "(2) - Historique" << endl;
    cout << "(3) - Statistiques" << endl;
    cout << "(q) Quitter l'application" << endl;
}

void console::selection_action()
{
    cout << endl << "Que voulez vous faire (tapez '?' pour de l'aide)? " ;
    
    char answer;
    cin >> answer;
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    
    switch(answer)
    {
        case '1':
            this->afficheInstantane();
            break;

        case '2':
            this->afficheHistorique();
            break;

        case '3':
            this->afficheStats();
            break;

        case 'q':
        case 'Q':
            this->quitter();
            return;

        case '?':
            this->afficheMenu();
            break;

        default:
            cout << "Erreur de saisie!" << endl;
            afficheMenu();
            break;
    }
    this->selection_action();
}

void console::afficheInstantane()
{
    cout << "Affichage de la dernière valeur enregistrée" << endl;
    repSaisie result = this->saisie(getStrSensor(), false);
    if(!result.data.is_null())
    {
        this->Console_to_Sensor(result);
    }
    
}

void console::afficheHistorique()
{
    cout << "Affichage de l'historique des 15 dernière minutes enregistrées" << endl;
    repSaisie result = this->saisie(getStrHisto());
    if(!result.data.is_null())
    {
        this->Console_to_Sensor(result);
    }
}

void console::afficheStats()
{
    cout << "Affichage de statistiques calculées sur la dernière heure enregistrée" << endl;
    repSaisie result = this->saisie(getStrStat(), true);
    if(!result.data.is_null())
    {
        if(result.nmCapt.empty())
        {
            this->Console_to_StatsAll(result);
        }
        else
        {
            
            this->Console_to_Stats(result);
        }
    }
}

string get_time_stamp_string(time_t time_stamp)
{
    tm * ptm = std::localtime(&time_stamp);
    char buffer[32];
    strftime(buffer, 32, "%d/%m/%Y %H:%M:%S", ptm);
    return string(buffer);
}

void affiche_dateRef(repSaisie result)
{
    if(result.commande.compare("sensors") == 0)
    {
        cout << "Le résultat a été trouvé à partir de la date de référence" << endl;
    }
    else if(result.commande.compare("histo") == 0)
    {
        cout << "Le résultat montre le 1/4 d'heure de données trouvé à partir de la date de référence" << endl;
    }
    else
    {
        cout << "Les statistiques sont calculées sur une heure de données à partir de la date de référence" << endl;
    }
     cout << "Date de référence: " << get_time_stamp_string(result.dateRef >= 0 ? result.dateRef : time(nullptr)) << endl;
}

void console::Console_to_Sensor(repSaisie result)
{
    int timeStamp = -1;  

    //Trouver le nom du capteur
    string capteur = "";
    string date = "";
    for(nljson value: result.data["values"])
    {
        //Les timeStamps sont ordonnés
        //On vérifie à chaque itération si on a la même date
        //Si non on affiche la nouvelle date 
        int nv_timeStamp=  value["time_stamp"];
        if(nv_timeStamp != timeStamp)
        {
            timeStamp=nv_timeStamp;
            date=value["date"];
            //On sépare l'affichage chaque date par 40 '='
            cout << endl << "=======================================" << endl << endl;
            cout << "Date de la capture: " << date << endl;
        }

        //Les values sont ordonnées par capteur en fonction des timestamps
        //On vérifie à chaque itération si on a le même capteur
        //Si non on affiche le nouveau capteur
        string nvCapteur = value["capteur"];
        if(capteur != nvCapteur)
        {
            capteur = nvCapteur;
            cout << endl << "--- Capteur: " << capteur << " ---" << endl << endl;
        }

        string nom = this->featuresMap[value["nom"]];
        double valeur = value["valeur"];
        
        cout << nom << ": " << valeur << this->featuresUnits[value["nom"]] << endl;
    }
    //On rajoute une ligne vide pour un affichage plus clair
    cout << endl;
    affiche_dateRef(result);
    this->saveFile(result);
}

void console::display_Stat(const nljson& value, string capteur)
{
    string nom = this->featuresMap[value["nom"]];
    string unit = this->featuresUnits[value["nom"]];
    double avg = value["avg"];
    double max = value["max"];
    double min = value["min"];
    int count = value["count"];
    cout << nom << ": " << endl;
    cout << "-----------" << endl;
    cout << "Valeur Max : " << max << unit << endl;
    cout << "Valeur Min : " << min << unit << endl;
    if(capteur == "HCSR501")
    {
        double taux = avg*100;
        cout << "Taux de présence: " << taux << "%"  << endl;
    }
    else
    {
        cout << "Moyenne: " << avg << unit << endl;
    }
    cout << "Nombre de données: " << count << endl << endl;
}

void console::Console_to_Stats(repSaisie stat)
{
    string capteur = stat.isPres? "HCSR501": stat.data["values"][0]["capteur"];
    cout << endl << "--- Capteur: " << capteur << " ---" << endl << endl;
    for(nljson value: stat.data["values"])
    {       
        if(!stat.isPres)
        {
            display_Stat(value, capteur);
        }
        else
        {
            int count = value["count"];
            string date = value["date"];
            double valeur = value["valeur"];

            string label = valeur == 0? "non-détection":"détection";

            cout << "Nombre de " << label << "s: " << count << endl;
            cout << "Date de la dernière " << label << ": " << date << endl << endl;
        }
    }

    cout << endl;
    affiche_dateRef(stat);
    this->saveFile(stat);
}

void console::Console_to_StatsAll(repSaisie stat)
{
    string capteur = "";
    for(nljson value: stat.data["values"])
    {
        string nvCapteur = stat.isPres ? "HCSR501" : value["capteur"];
        if(capteur.compare(nvCapteur) != 0)
        {
            capteur = nvCapteur;
            cout << endl << "--- Capteur: " << capteur << " ---" << endl;
        }
        display_Stat(value, capteur);
    }
    cout << endl;
    affiche_dateRef(stat);
    this->saveFile(stat);
}

void console::saveFile(repSaisie result)
{
    if(result.saveFile)
    {
        time_t now = std::time(NULL);
        tm * ptm = std::localtime(&now);
        char buffer[32];
        std::strftime(buffer, 32, "%Y%m%d_%H%M%S", ptm);
        stringstream s;
        s << result.commande << "_" << buffer << ".json";
        string fileName = s.str();
        ofstream file(fileName);
        file << result.data.dump(4);
        cout << "Les données ont été enregistrées dans le fichier '"<< fileName <<"'" << endl;
    }
}

bool validateDate(const std::string& date) {
    std::regex dateFormat("\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}");
    return std::regex_match(date, dateFormat);
}

repSaisie console::saisie( string commande , bool isStat)
{
    repSaisie result;
    result.commande = commande;
    char repFichier;
    cout << "Renseigner un nom de capteur (" << this->getCapteurs() << ") ou tapez <Enter> pour tous les capteurs: ";
    getline(cin, result.nmCapt);
    string date;
    cout << "Date de référence (jj/mm/aaaa HH:MM:SS) ou vide pour la dernière date enregistrée: ";
    bool valide =false;
    do
    {
         getline(cin, date);
         valide = date.empty() || validateDate(date);
         if(!valide)
         {
            cout << "Date invalide recommencez (format jj/mm/aaaa HH:MM:SS): ";
         }
    } while (!valide);
     
    if(date.empty())
    {
        result.dateRef = -1;
    }
    else
    {
        time_t now = time(nullptr);
        struct std::tm tm = *localtime(&now);
        std::istringstream ss(date);
        ss >> std::get_time(&tm, "%d/%m/%Y %H:%M:%S");
        result.dateRef = mktime(&tm);
    }
    
    cout << "Enregister les données (o/n)? ";
    cin >> repFichier;
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    result.saveFile = toupper(repFichier) == 'O';
    stringstream req = stringstream();
    req << getAdresse(commande);
    for(long unsigned int i = 0; i < result.nmCapt.length(); i++)
        result.nmCapt[i] = toupper(result.nmCapt[i]);
    
    result.isPres = isStat && result.nmCapt == "HCSR501";

    if(!result.nmCapt.empty())
    {
        req << result.nmCapt;
    }
    if(result.dateRef >=0)
    {
        req << "?time_stamp=" << result.dateRef;
    }
    result.data = sendRequest(req.str());
    
    return result;
}

void console::quitter()
{
    cout << "Merci d'avoir utilisé l'application.  À bientôt!" << endl;
    exit(0);
}

string console::getCapteur()
{
    string nmCapt;
    cin >> nmCapt;

    return nmCapt;

}

string console::getAdresse(string command)
{
    stringstream s;
    s << this->endPoint << command << "/";
    return s.str();
}

string console::getStrSensor()
{
    return "sensors";
}

string console::getStrHisto()
{
    return "histo";
}

string console::getStrStat()
{
    return "stats";
}

