#include "app.hpp"
using nljson = nlohmann::json_abi_v3_11_2::json;


void set_default_config(nljson& config)
{
    config["ip"] = "57.128.34.248";
    config["port"]= 1975;
}

void app::loadConfig(nljson &config)
{
    set_default_config(config);
    std::ifstream f(app::config_filename);
    if (f.fail())
    {
        PLOGD << "Fichier de configuration introuvable : utilisation des valeurs par défaut";
    }
    else
    {
        PLOGI << "Ouverture du fichier de configuration";
        nljson config_patch = nljson::parse(f, nullptr, false);
        if (config.is_discarded())
        {
            PLOGW << "Erreur lors de la lecture du fichier de configuration: utilisation des valeurs par défaut";
        }
        else
        {
            config.merge_patch(config_patch);
        }
    }
}

bool app::init(int argc, char* argv[])
{
    try
    {
        init_logs(argc >1 ? string(argv[1]): string(""));
        PLOGI << "Initialisation du programme";
        nljson config;
        loadConfig(config);
        return csl.init(config);
        return true;
    }
    catch(const std::exception& e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
    
    
}

void app::run()
{
    PLOGI << "Lancement du programme";
    try
    {
        this->csl.run();
    }
    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }
    
    this->exit();
}

void app::clean()
{
    PLOGD << "Clean du programme";
}

void app::exit()
{
    PLOGW << "Sortie  du programme";
}