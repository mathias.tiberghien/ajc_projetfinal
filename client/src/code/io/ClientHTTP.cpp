#include "clientHTTP.hpp"

nljson sendRequest(string httpAdr)
{
    PLOGD << "adresse " << httpAdr;
    nljson result;
    try{
    // création du client Http d'après la requète demandée
    http_client client(httpAdr);
    
    // Send a GET request without a key
    // This should return all of the data as a JSON object
    

       client.request(methods::GET)
        .then([](http_response response)
        {
            // Print the status code and the response body
            PLOGD << "GET: " << response.status_code();
            return response.extract_json();
        })
        .then([&result](json::value body)
        {
            result = nljson::parse(body.serialize());
            if(result.is_null())
            {
                 PLOGW << "Aucun capteur avec ce nom !!";
            }
        }).wait(); 
    }
    catch(exception &ex)
    {
        PLOGE << "Erreur lors de la récupération des données: " << ex.what();
    }
    return result;
}