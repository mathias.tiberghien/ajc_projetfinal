#include "app.hpp"

bool app::init(int argc, char* argv[])
{
    try
    {
        init_logs(argc >1 ? string(argv[1]): string(""));
        PLOGI << "Démarrage de l'application master";
        PLOGI << "Le rôle de cette application est de recevoir les données de la balise,";
        PLOGI << "de les formater en format json et de les transmettres.";
        json config;
        this->loadConfig(config);
        bool initOk = serial.init(config);
        if(initOk)
        {
            initOk = this->messageHandler.init(config);
        }
        
        return initOk;
        
    }
    catch(const std::exception& e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
    
    
}

void set_default_config(json& config)
{
    config["frequence"] = 3;
    config["pca9685"]["bus"] = 1;
    config["pca9685"]["adresse"] = 0x40;
    config["tcp"]["ip"] = "57.128.34.248";
    config["tcp"]["port"] = 5000;
    config["serial"]["device"] = "/dev/serial0";
    config["serial"]["baud"] = 9600;
}

void app::loadConfig(json &config)
{
    set_default_config(config);
    std::ifstream f(app::config_filename);
    if (f.fail())
    {
        PLOGD << "Fichier de configuration introuvable : utilisation des valeurs par défaut";
    }
    else
    {
        PLOGI << "Ouverture du fichier de configuration";
        json config_patch = json::parse(f, nullptr, false);
        if (config_patch.is_discarded())
        {
            PLOGW << "Erreur lors de la lecture du fichier de configuration: utilisation des valeurs par défaut";            
        }
        else
        {
            config.merge_patch(config_patch);
        }
    }
    int freq = config["frequence"];
    if(freq < 3)
    {
        config["frequence"] = 3;
        PLOGW << "La période entre deux mesures doit être supérieure à 3 secondes";
    }

    PLOGD << "la frequence utilisée est : " << (int)config["frequence"];
}

void app::startReceiveLoop()
{
    PLOGW << "Démarrage du service de réception des messages";
    pthread_create (&thread, NULL, receiveAndHandleLoop, this);
}

void app::stopReceiveLoop()
{
    pthread_cancel(thread);
    void *result;
    pthread_join(thread, &result);
    PLOGW << "Arrêt du service de réception des messages.";
}

void* receiveAndHandleLoop(void* arg)
{
    app* this_app = (app*)arg;
     // activer les signaux d'annulation
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    // annuler le thread de manière asynchrone
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    if(this_app->messageHandler.connectToServeur())
    {
        while(true)
        {
            this_app->serial.wait_for_data();
            vector<string> messages;
            do
            {      
                string message = this_app->serial.receive();
                messages.push_back(message);
                this_app->messageHandler.handlePresence(message);
            } while (this_app->serial.has_data());
            this_app->messageHandler.handleMessageBatch(messages);
            messages.clear();
        }
    }
    return 0;
}

void app::run()
{
    PLOGW << "Lancement du programme";
    try
    {
        this->startReceiveLoop();
        char saisie;
        while(true)
        {
            cout << "Tapez q pour quitter:" << endl;
            cin >> saisie;
            if(saisie == 'q')
            {
                break;
            }
        }
        this->stopReceiveLoop();

    }
    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }
    this->exit();
}

void app::clean()
{
    PLOGD << "Clean du programme";
    serial.close();
    messageHandler.clean();
}

void app::exit()
{
    PLOGW << "Sortie  du programme";
}