#include "messagehandler.hpp"

void MessageHandler::setSleeps(int frequence)
{
    int tiers = frequence/3;
    this->sleep1 = frequence%3 > 1 ? tiers + 1 : tiers;
    this->sleep2 = frequence%3 > 0 ? tiers + 1 : tiers;
    this->sleep3 = tiers;
}

bool MessageHandler::init(const json& config)
{
    bool result = false;
    this->setSleeps(config["frequence"]);
    result = this->controlleur.init(config);
    this->setEtatPresence(Yellow_State::Init);
    this->setEtatTransmission(Blue_State::Rest);
    bool tInitOk = this->transmitter.init(config);
    return result && tInitOk;
}

void MessageHandler::clean()
{
    this->controlleur.moveBlueFlag(Blue_State::None);
    this->controlleur.moveYellowFlag(Yellow_State::Init);
    this->transmitter.clean();
    this->controlleur.clean();
}
        
void MessageHandler::handleMessageBatch(vector<string> messages)
{
    if(this->isDetection1)
    {
        this->setEtatTransmission(Blue_State::Detection1);
    }
    else
    {
        this->setEtatTransmission(Blue_State::Detection2);
    }
    this->isDetection1 = !this->isDetection1;
    sleep(this->sleep1);
    this->sendMessages(messages);
    sleep(this->sleep2); 
    this->setEtatTransmission(Blue_State::Rest);
    sleep(this->sleep3);
}

void MessageHandler::handlePresence(string message)
{
    json message_json = json::parse(message, nullptr, false);
    if(!message_json.is_discarded())
    {
        if(message_json["capteur"] == "HCSR501")
        {
            bool presence = message_json["valeur"] == 1;
            PLOGD << "Présence évaluée à: " << presence;
            if(presence)
            {
                this->setEtatPresence(Yellow_State::Presence);
            }
            else
            {
                if(this->etat_presence == Yellow_State::Presence)
                {
                    this->setEtatPresence(Yellow_State::NoPresence);
                }
            }
        }
    }
    else
    {
        PLOGD << "Mauvais message commençant par le charactère ascii: " << (int)message[0];
    }
}

void MessageHandler::sendMessages(vector<string> messages)
{
    this->setEtatTransmission(Blue_State::Transfert);
    for(string message: messages)
    {
        this->transmitter.send(message);
        usleep(500);
    }      
}

void MessageHandler::setEtatPresence(Yellow_State state)
{
    if(state != this->etat_presence)
    {
        PLOGD << "Mise du drapeau jaune à : " << state << "°";
        this->etat_presence = state;
        this->controlleur.moveYellowFlag(state);
    }
}

void MessageHandler::setEtatTransmission(Blue_State state)
{
    if(state != this->etat_transmission)
    {
        PLOGD << "Mise du drapeau bleu à : " << state << "°";
        this->etat_transmission = state;
        this->controlleur.moveBlueFlag(state);
    }
}

bool MessageHandler::connectToServeur()
{
    return this->transmitter.connectToServer();
}