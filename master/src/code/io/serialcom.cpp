# include "serialcom.hpp"

bool SerialCom::init(const json& config)
{
    try
    {
        PLOGD << "Initialisation du transmetteur";
        this->device = config["serial"]["device"];
        this->baudRate = config["serial"]["baud"];
        this->fd = serialOpen(this->device.c_str(), this->baudRate);
        bool initOk = this->fd >=0;
        if(initOk)
        {
            serialFlush(this->fd);
        }
        log_init("Transmetteur TX", initOk);
        return initOk;
    }
    catch(const std::exception& e)
    {
        PLOGE << e.what() << '\n';
    }
    return false;
    
}

void SerialCom::wait_for_data()
{
    while(!this->has_data())
    {

    }
}

bool SerialCom::has_data()
{
    return serialDataAvail(this->fd);
}

void SerialCom::close()
{
    PLOGD << "Fermeture du transmetteur";
    serialClose(this->fd);
}

void SerialCom::send(string message)
{
    stringstream s;
    s << message << endl;
    PLOGD << "Transmission du message: " << message;
    serialPuts(this->fd, s.str().c_str());

}

string SerialCom::receive()
{
    stringstream message;
    if(serialDataAvail(this->fd))
    {
        char c;
        while(c!='\n')
        {
            c = (char)serialGetchar(this->fd);
            if(c!='\n')
            {
                message << c;
            }
        }
        PLOGD << "Reçu de la balise: " << message.str();
    }

    return message.str();
}