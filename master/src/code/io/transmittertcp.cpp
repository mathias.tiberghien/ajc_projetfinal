#include "transmittertcp.hpp"

bool TransmitterTCP::init(const json& config)
{
    bool initOk = true;
    this->port = config["tcp"]["port"];
    this->ip = config["tcp"]["ip"];
    this->sock_client = socket(AF_INET, SOCK_STREAM, 0);
    if(sock_client < 0)
    {
        PLOGW << "La création du socket a échouée!";
        initOk = false;
    }
   
    log_init("Transmetteur TCP", initOk);

    return initOk;
}

bool TransmitterTCP::connectToServer()
{
    bool connected = false;
    sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(this->port);
    serv_addr.sin_addr.s_addr = inet_addr(this->ip.c_str());
    PLOGI << "Connexion au Serveur...";
    int i = 1;
    int max = 10;
    while(!connected && i<=max)
    {
        connected = connect(sock_client, (sockaddr*)&serv_addr, sizeof(serv_addr)) >=0;
        PLOGI << "Tentative " << i++ << " sur " << max;
        if(!connected)
        {
            PLOGW << "La tentative de connexion a échoué. Essai dans 3 secondes";
            sleep(3);
        }   
    }
    if(!connected)
    {
        PLOGF << "La connexion au serveur a échoué. L'application va fermer.";
    }
    else
    {
        PLOGI << "Connecté au Serveur TCP";
    }

    return connected;
}

void TransmitterTCP::send(string message)
{
    char msg[TAILLE_BUFFER];
    memset(msg,0,TAILLE_BUFFER);
    strcpy(msg, message.c_str());
    int sendRes = ::send(this->sock_client, msg, TAILLE_BUFFER, 0);
    if(sendRes < 0)
    {   
        PLOGF << "Echec de l'envoi!!\r\n";
    }
    else
    {
        PLOGD << "Envoyé: " << message;
    }
}

void TransmitterTCP::clean()
{
    if(this->sock_client >=0)
    {
        PLOGD << "Envoi exit avec socket qui vaut :" << sock_client;
        this->send("exit");
    }
    close(this->sock_client);
}
