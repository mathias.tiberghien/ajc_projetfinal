#pragma once

#include "pipca9685.h"
#include "log.hpp"
#include "common.hpp"
#include "transmittertcp.hpp"

using json = nlohmann::json_abi_v3_11_2::json;

/**
 * @brief enumération des différents état du drapeau jaune
 * chaque état correspondant à un angle dans lequel on peu défnir le drapeau
 */
enum Yellow_State{Init = 0, Presence = 90, NoPresence = 180};
/**
 * @brief enumération des différents état du drapeau jaune
 * chaque état correspondant à un angle dans lequel on peu défnir le drapeau
 */
enum Blue_State{None = 0, Detection1 = 45, Transfert = 90, Detection2 = 135, Rest = 180};

/**
 * @brief cette classe s'occupe de la gestion des messages TCP sur le master
 * Il va analyser les messages contenant les mesures reçu de la balise. 
 * En fonction desdit messages gérer les position des drapeaux jaunes et bleus
 * et enfin renvoyer les données au serveur TCP à traver une instance de transmitterTCP
 */
class MessageHandler
{

    public:

    /**
     * @brief Constructeur de la classe MessageHandler, on y définit les constante interne de manière automatique.
     * 
     */
        MessageHandler():etat_presence(Yellow_State::Presence),
            sleep1(1), sleep2(1), sleep3(1),isDetection1(true),
            etat_transmission(Blue_State::Detection1), controlleur(1, 0x40),
            transmitter("57.128.34.248", 5000){};
    /**
     * @brief lance l'initialisation des connection vers le micro controleur et le serveur TCP
     * 
     * @param config contient la configuration des connections
     * @return true si lst connecté à l'issue de l'initialisation, false sinon.
     */
        bool init(const json& config);
        /**
         * @brief nettoie la classe et fermes les connections vers le micro controlleur et le serveur TCP
         * à traver les objets privés controlleur et transmitter
         * 
         */
        void clean();
        /**
         * @brief gère le traitement des données issus de la balise, détermine
         * la mosition des drapeaux et renvois les données au Serveur.
         * 
         * @param messages 
         */
        void handleMessageBatch(vector<string> messages);
        /**
         * @brief Gère le drapeau jaune en cas de détection de présence
         * 
         * @param message 
         */
        void handlePresence(string message);
        bool connectToServeur();
    private:
        void setEtatPresence(Yellow_State state);
        void setEtatTransmission(Blue_State state);
        void sendMessages(vector<string> messages);
        void setSleeps(int frequence);
    Yellow_State etat_presence;
    int sleep1;
    int sleep2;
    int sleep3;
    bool isDetection1;
    Blue_State etat_transmission;
    PiPCA9685 controlleur;
    TransmitterTCP transmitter;

};