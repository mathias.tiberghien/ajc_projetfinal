
#ifndef PIPCA9685_H
#define PIPCA9685_H

#include <unistd.h>
#include <math.h>
#include "log.hpp"
#include "i2c.h"
#include "common.hpp"

#define MODE1			0x00
#define MODE2			0x01
#define SUBADR1			0x02
#define SUBADR2			0x03
#define SUBADR3			0x04
#define PRESCALE		0xFE
#define LED0_ON_L		0x06
#define LED0_ON_H		0x07
#define LED0_OFF_L		0x08
#define LED0_OFF_H		0x09
#define ALL_LED_ON_L    0xFA
#define ALL_LED_ON_H	0xFB
#define ALL_LED_OFF_L	0xFC
#define ALL_LED_OFF_H	0xFD
// Bits
#define RESTART			0x80
#define SLEEP			0x10
#define ALLCALL			0x01
#define INVRT			0x10
#define OUTDRV			0x04

#define PAN			    0
#define TILT			1
#define FREQUENCY		50
#define CLOCKFREQ		25000000
#define PANOFFSET		1
#define PANSCALE		1.4
#define TILTOFFSET		30
#define TILTSCALE		1.43
#define PANMAX			270
#define PANMIN			90
#define TILTMAX			90
#define TILTMIN			-45

/**
 * @brief Cette classe s'occupe de la connection au micro controlleur PiPCA9685
 * Le protocole i2c est utilisé pour communiquer avec la carte.
 * Sur cette carte sont branchés deux servo-moteur actionnant des drapeaux
 * un blue et un jaune 
 * 
 */
class PiPCA9685
{
public:
    /**
    * @brief Constructeur de la classe PiPCA9685
    * la connection avec le microcontrolleur n'est pas initialisé
    * 
    * @param i2c_bus bus i2c sur lequel est branché le micro-controller
    * @param i2c_address adresse i2c du micro-controlleur
    */
    explicit PiPCA9685(uint8_t i2c_bus, uint8_t i2c_address);
    ~PiPCA9685();
    /**
     * @brief initialise la connection TCP du master vers le micro controlleur
     * 
     * @param config contient la configuration de la connection 
     * @return true si l'iniatlisation s'est bien passée, false sinon.
     */
    bool init(const json& config);
    /**
     * @brief méthode pour déconnecté le micro controlleur
     */
    void clean();
    /**
     * @brief retourne la valeur d'erreur de la connection.
     * @warning true indique la présence d'une erreur !
     * 
     * @return la valeur error
     */
    bool getError();
    /**
     * @brief renvoie le numéro du bus i2c sur lequel est connecté le micro controlleur
     * 
     * @return uint8_t numéro du bus 
     */
    uint8_t getBus();
        /**
     * @brief renvoie le numéro de l'adresse i2c sur lequel est connecté le micro controlleur
     * 
     * @return uint8_t numéro de l'adresse 
     */
    uint8_t getAdresse();
    /**
     * @brief renvoie le nom du microcontrolleur
     * 
     * @return const char* nom du microcontrolleur
     */
    const char * getNom() {return nom;};

    /**
     * @brief actionne le drapeau jaune vers la position passée en paramêmtre en degré
     * 
     * @param deg position à atteindre
     */
    void moveYellowFlag(int deg);
    /**
     * @brief actionne le drapeau bleu vers la position passée en paramêmtre en degré
     * 
     * @param deg position à atteindre
     */
    void moveBlueFlag(int deg);

private :
    void setPwmFrequency(uint16_t frequency);
    void setPwm(uint8_t channel, uint16_t on, uint16_t off);
    void setAllPwm(uint16_t on, uint16_t off);
    void move(uint8_t channel , int deg);



    uint8_t i2c_bus;
    uint8_t i2c_address;
    const char* nom;

    const uint8_t yellowFlagChannel =1;
    const uint8_t blueFlagChannel = 2;


    i2c *deviceI2C;

    bool error;
};

#endif // PIPCA9685_H