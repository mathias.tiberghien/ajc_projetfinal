#pragma once
#include "common.hpp"
#include "log.hpp"
#include "serialcom.hpp"
#include "messagehandler.hpp"
#include <pthread.h>
#include <unistd.h>

/**
 * @brief Dans cette classe est décrit les différentes étapes de l'execution du progamme
 * de l'initialisation et de la configuration de ses composants à sa fermeture.
 * 
 */
class app
{
    public:
        //On définie une config par défaut à l'initialisation pour ne pas avoir de valeur non définie
        /**
        * @brief Constructeur de la classe app.
        * on y définit le nom du port de communication Rx/Tx
        * et le nom par defaut de fichier de configuration
        */
        app(): serial("/dev/serial0"), config_filename("./app_config.json"){} //la carte à l'adresse 0x40 sur le bus i2c numéro 1
        /**
         * @brief destructeur de la classe app
         */
        ~app(){this->clean();}
        /**
         * @brief charge la configuration à partir d'un fichier .json, 
         * lance l'initialisation des différents capteur 
         * et initialise la connection Rx/Tx
         * 
         * @param argc nombre de parametre au lancement du programme
         * @param argv liste des parametres passés au programme lors de son lancement
         * @return true si l'initialisation est correcte, false sinon
         */
        bool init(int argc, char* argv[]);
        /**
        * @brief lance l'execution du programme celui-ci se fait dans second thread 
        *  il est possible de taper q à tous moment pour quitter le programme.
        */
        void run();
    private:
        void clean();
        void exit();
        void startReceiveLoop();
        void stopReceiveLoop();
        void loadConfig(json& config);
        void setPeriodeRepos(int secondes);
        SerialCom serial;
        MessageHandler messageHandler;
        pthread_t thread;
        const string config_filename;  
        friend void* receiveAndHandleLoop(void*);
};
/**
 * @brief défini et créer un thread dans lequel va s'executer le programme.
 * cette fonction va aussi définir le signal de fin du thread.
 * 
 * @return renvoie 0 en fin de fonction, le retour n'est pas sensé être atteind
 */
extern void* receiveAndHandleLoop(void*);