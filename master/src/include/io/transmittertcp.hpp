#pragma once

#include <string>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include "common.hpp"
#include "log.hpp"
#include "json.hpp"

#define TAILLE_BUFFER 2000

/**
 * @brief Cette classe s'occupe de la communication TCP du master vers le serveur receuillant les informations.
 * les données envoyées aux serveur sont des chaines de characters représentant les données au format json
 * la communication n'est utilisée que du master vers le serveur
 */
class TransmitterTCP
{
    public:
        /**
         * @brief Constructeur de la classe TransmitterTCP, n'initialise pas la connection
         * 
         * @param ip IP du serveur
         * @param port port ouvert sur le serveur.
         */
        TransmitterTCP(string ip, int port=80):ip(ip),port(port), sock_client(-1){};
        /**
         * @brief initialise la connection TCP en client, 
         * Le serveur doit être à l'écoute de nouvelle connection pour que le master puisse s'y connecter
         * 
         * @param config configuration de la connection (port et IP)
         * @return true si la connection s'est bien passée, false sinon.
         */
        bool init(const json& config);
        /**
         * @brief Envoi un message sous forme de chaine de character vers le serveur TCP
         * 
         * @param message 
         */
        void send(string message);
        /**
         * @brief ferme la connection avec le serveur, si la socket est toujours fonctionnelle
         * on envoie "exit" au serveur pour le prévenir de la fin de la connection.
         */
        void clean();

        bool connectToServer();
    private:
        string ip;
        int port;
        int sock_client;
};