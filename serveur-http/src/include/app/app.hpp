#pragma once
#include "common.hpp"
#include "log.hpp"
#include "server_http.hpp"



class app
{
    public:
        app(): config_filename("./app_config.json"){}
        ~app(){this->clean();}
        bool init(int argc, char* argv[]);
        void run();
    private:
        void clean();
        void exit();
        void loadConfig(nljson& config);
        const string config_filename;
        ServerHttp http;
};