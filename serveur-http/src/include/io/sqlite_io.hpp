#ifndef SQLITE_IO_HPP_INCLUDED
#define SQLITE_IO_HPP_INCLUDED

#include "db_models.hpp"

typedef enum Query_Type{SELECT, INSERT, UPDATE, DELETE} Query_Type;

class Db
{
    public:
    Db();
    ~Db();

    bool connect(string path);
    bool init_query(Query_Type, Db_template& o,vector<string> field_selection = {}, const char* condition = NULL, int limit=0);
    bool begin_transaction();
    bool commit_transaction();
    bool rollback_transaction();
    bool get_next(Db_template& o);
    bool exec_custom_query_no_return(string query);
    bool exec_insert(Db_template& o, bool useIds=false);
    bool exec_delete();
    bool exec_update(Db_template& o);
    string get_query(){return query;};
    void finalize_query();
    void disconnect();
    vector<string> get_query_columns(){return this->select_columns;}
    private:
        bool execute_query(const char* query);
        string path;
        sqlite3* dbo;
        sqlite3_stmt* stmt;
        Query_Type query_type;
        vector<string> select_columns;
        string query;
};

#endif // SQLITE_IO_HPP_INCLUDED
