#pragma once

#include "common.hpp"
#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#include <cpprest/uri.h>
#include <iostream>
#include "log.hpp"
#include "json_db_model.hpp"
#include "sqlite_io.hpp"

using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

extern string make_http(string ip, int port);


struct DB_Query
{
    DB_Query(): fields({}), condition(string("")), limit(0){};
    ~DB_Query(){};
    vector<string> fields;
    string condition;
    int limit;
};

class ServerHttp
{
    public:
        ServerHttp(): ip(""), port(-1), db_file_path("./balise.db")
        {
            nljson data;
            data["capteur"]="";
            data["nom"]="";
            data["time_stamp"]=(long)0;
            data["valeur"]= (double)0;
            this->db_template = Json_Db_Template("data", {"capteur", "nom", "time_stamp"}, data);
        };
        ~ServerHttp(){};
        bool init(const nljson& config);
        void close();

    private:
        void handle_get(http_request request);
        DB_Query build_query_sensors(const vector<string>& paths,map<string,string>& queryArgs);
        DB_Query build_query_histo(const vector<string>& paths,map<string,string>& queryArgs);
        DB_Query build_query_stats(const vector<string>& paths,map<string,string>& queryArgs);
        DB_Query build_query(const vector<string>& paths,map<string,string>& queryArgs);
        string ip;
        int port;
        string db_file_path;
        http_listener listener;
        Json_Db_Template db_template;
};

