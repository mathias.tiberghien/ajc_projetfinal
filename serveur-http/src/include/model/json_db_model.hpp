#pragma once

#include "json.hpp"
#include "db_models.hpp"
#include <list>
#include "log.hpp"

using nljson = nlohmann::json_abi_v3_11_2::json;

class Json_Db_Template: public Db_template
{
    public:
        Json_Db_Template(){};
        Json_Db_Template(string name, vector<string> keys,const nljson& data, vector<string> ignored={});
        virtual void read_from_stmt(sqlite3_stmt* stmt, vector<string> columns) override;
        virtual void bind_to_stmt(sqlite3_stmt* stmt, vector<string> column_selection, bool is_update) override;
        virtual void clean_model() override;
        virtual string to_string() override;
        nljson& getData(){return this->data;};
    private:
        nljson data;
        nljson query_data;
        map<string,bool> is_column_number;
        vector<char*> binding_values;
};

extern string get_string_or_empty(const unsigned char* text);