#ifndef DB_MODELS_HPP_INCLUDED
#define DB_MODELS_HPP_INCLUDED


#include <iostream>
#include <vector>
#include <sstream>
#include <sqlite3.h>

using namespace std;

class Db_template
{
    public:
        Db_template(){}
        ~Db_template(){};
        vector<string> get_properties() const {return db_properties;};
        vector<string> get_keyProperties() const {return db_keyProperties;};
        string get_name() const {return db_name;}
        virtual void read_from_stmt(sqlite3_stmt* stmt, vector<string> columns)=0;
        virtual void bind_to_stmt(sqlite3_stmt* stmt, vector<string> column_selection, bool is_update)=0;
        virtual void clean_model() = 0;
        virtual string to_string() = 0;
    protected:
        void set_name(string name){db_name = name;}
        void add_property(string p){db_properties.push_back(p);}
        void add_keyProperty(string p){db_keyProperties.push_back(p);}
    private:
        string db_name;
        vector<string> db_properties;
        vector<string> db_keyProperties;
};



string to_safe_field(string s);
string build_query_select(const Db_template& db_obj,vector<string> field_selection, const char* condition, int limit=0);
string build_query_insert(const Db_template& db_obj,vector<string> field_selection);
string build_query_delete(const Db_template& db_obj, const char* condition);
string build_query_update(const Db_template& db_obj,vector<string> field_selection, const char* condition);

#endif // DB_MODELS_HPP_INCLUDED
