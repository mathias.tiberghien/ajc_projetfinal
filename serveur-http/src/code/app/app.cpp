#include "app.hpp"

void set_default_config(nljson& config)
{
    config["ip"] = "57.128.34.248";
    config["port"] = 1975;
    config["db_path"] = "./balise.db";
}

void app::loadConfig(nljson &config)
{
    set_default_config(config);
    std::ifstream f(app::config_filename);
    if (f.fail())
    {
        PLOGD << "Fichier de configuration introuvable : utilisation des valeurs par défaut";
    }
    else
    {
        PLOGI << "Ouverture du fichier de configuration";
        nljson config_patch = nljson::parse(f, nullptr, false);
        if (config.is_discarded())
        {
            PLOGW << "Erreur lors de la lecture du fichier de configuration: utilisation des valeurs par défaut";
        }
        else
        {
            config.merge_patch(config_patch);
        }
    }
    PLOGD << "Le fichier de base de données est " << config["db_path"];
}

bool app::init(int argc, char* argv[])
{
    try
    {
        init_logs(argc >1 ? string(argv[1]): string(""));
        nljson config;
        this->loadConfig(config);
        return this->http.init(config);
    }
    catch(const std::exception& e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
    
    
}

void app::run()
{

    PLOGI << "Tapez q pour quitter:";
    std::string line;
    while (std::getline(std::cin, line) && line.substr(0,1)!="q");
    
    this->exit();
}

void app::clean()
{
    PLOGD << "Clean du programme";
}

void app::exit()
{
    this->http.close();
    PLOGW << "Sortie  du programme";
}