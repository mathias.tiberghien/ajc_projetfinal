#include "server_http.hpp"

bool ServerHttp::init(const nljson& config)
{
    this->port = config["port"];
    this->ip = config["ip"];
    this->db_file_path = config["db_path"];
    this->listener = http_listener(make_http(this->ip, this->port));

    this->listener.support(methods::GET, std::bind(&ServerHttp::handle_get, this, std::placeholders::_1));
    auto status = this->listener.open().wait();
    bool initOk = status == pplx::task_status::completed;
    log_init("Listener HTTP", initOk);
    PLOGI_IF(initOk) << "A l'écoute à l'adresse " << this->ip << " sur le port " << this->port;
    return initOk;

}

void ServerHttp::close()
{
    PLOGW << "Fermeture du listener http";
    bool ok = this->listener.close().wait() == pplx::task_status::completed;
    PLOGW_IF(!ok) << "Le listener n'a pas été fermé correctement";
    PLOGD_IF(ok) << "Le listener a été fermé correctement";
}

string get_time_stamp_string(time_t time_stamp)
{
    tm * ptm = std::localtime(&time_stamp);
    char buffer[32];
    strftime(buffer, 32, "%d/%m/%Y %H:%M:%S", ptm);
    return string(buffer);
}

DB_Query ServerHttp::build_query_sensors(const vector<string>& paths, map<string,string>& queryArgs)
{
    DB_Query result;
    string t_string = queryArgs["time_stamp"];
    long time_stamp = t_string.empty() ? time(NULL) + 3600 : stol(t_string.c_str());
    result.fields = {"capteur","nom", "strftime('%d/%m/%Y %H:%M:%S', time_stamp, 'unixepoch', '+1 hour') as date", "time_stamp", "valeur"};
    stringstream c;
    c << "time_stamp = (select time_stamp from data where time_stamp <= " << time_stamp << " order by time_stamp desc limit 1)";
    if(paths.size()>1)
    {
        c << " and capteur like '" << paths[1] << "'";
    }
    c << " ORDER BY time_stamp ASC, capteur ASC, nom ASC";
    result.condition = c.str();
    return result;
}

DB_Query ServerHttp::build_query_histo(const vector<string>& paths,map<string,string>& queryArgs)
{
    DB_Query result;
    string t_string = queryArgs["time_stamp"];
    long time_stamp = t_string.empty() ? time(NULL) + 3600 : stol(t_string.c_str());
    result.fields = {"capteur","nom", "strftime('%d/%m/%Y %H:%M:%S', time_stamp, 'unixepoch', '+1 hour') as date", "time_stamp", "valeur"};
    stringstream c;
    c << "(time_stamp >= ((select time_stamp from data where time_stamp <= " << time_stamp << " order by time_stamp desc limit 1) - 900) and time_stamp <= "  << time_stamp <<")";
    if(paths.size()>1)
    {
        c << " and capteur like '" << paths[1] << "'";
    }
    c << " ORDER BY time_stamp ASC, capteur ASC, nom ASC";
    result.condition = c.str();
    return result;
}

DB_Query ServerHttp::build_query_stats(const vector<string>& paths,map<string,string>& queryArgs)
{
    DB_Query result;
    string id = paths.size()>1 ? paths[1] : "";
    stringstream c;
    string t_string = queryArgs["time_stamp"];
    long time_stamp = t_string.empty() ? time(NULL) + 3600 : stol(t_string.c_str());
    bool presence = id.compare("presence") == 0 || id.compare("HCSR501") == 0;
    if(presence)
    {
        result.fields = {"valeur", "count(valeur) as count", "strftime('%d/%m/%Y %H:%M:%S', max(time_stamp), 'unixepoch', '+1 hour') as date"};
    }
    else
    {
        result.fields = {"capteur","nom", "min(valeur) as min",
         "max(valeur) as max", 
         "avg(valeur) as avg",
          "count(*) as count",
          "strftime('%d/%m/%Y %H:%M:%S', max(time_stamp), 'unixepoch', '+1 hour') as date"};
    }
    c << "(time_stamp >= ((select time_stamp from data where time_stamp <= " << time_stamp << " order by time_stamp desc limit 1) - 3600) and time_stamp <= "  << time_stamp <<")";
    if(paths.size()>1)
    {
        if(presence)
        {
            c << " and capteur like 'HCSR501'";
        }
        else
        {
            c << " and capteur like '" << paths[1] << "'";
        }
    }
    if(presence)
    {
        c << " GROUP by valeur";
    }
    else
    {
        c << " GROUP by capteur, nom";
    }
    
    result.condition = c.str();
    return result;
}

DB_Query ServerHttp::build_query(const vector<string>& paths,map<string,string>& queryArgs)
{
    string command = paths[0];
    if(command.compare("sensors")==0)
    {
        return this->build_query_sensors(paths, queryArgs);
    }
    else if(command.compare("histo") == 0)
    {
        return this->build_query_histo(paths, queryArgs);
    }
    else
    {
        return this->build_query_stats(paths, queryArgs);
    }
}

utility::string_t read_html_file(const utility::string_t& path) {
    std::ifstream file(path);
    std::stringstream buffer;
    buffer << file.rdbuf();
    return utility::conversions::to_string_t(buffer.str());
}


void ServerHttp::handle_get(http_request request)
{
    http_response server_response (status_codes::OK);
    server_response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
    auto paths = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    auto queryArgs = uri::split_query(uri::decode(request.request_uri().query()));
    vector<string> handled_paths = {"sensors", "histo", "stats"};
    if(paths.empty())
    {
        request.reply(status_codes::OK, read_html_file(U("./index.html")), U("text/html"));
    }
    server_response.headers().set_content_type(U("application/json"));
    bool has_handle = !paths.empty() && find(handled_paths.begin(), handled_paths.end(), paths[0]) != handled_paths.end();
    if(has_handle)
    {
        Db db;
        if(db.connect(this->db_file_path))
        {
            DB_Query query = this->build_query(paths, queryArgs);
            if(db.init_query(Query_Type::SELECT, this->db_template, query.fields, query.condition.c_str()))
            {
                json::value response;
                
                string q_string = db.get_query();
                PLOGD << "Query :" << q_string;
                int i=0;
                while(db.get_next(this->db_template))
                {
                    response["values"][i++]= json::value::parse(this->db_template.to_string());
                }
                if(i>0)
                {    
                    response["time_stamp"] = json::value::string(get_time_stamp_string(time(NULL) + 3600));
                    response["args"]= json::value::string("time_stamp:"+queryArgs["time_stamp"]);
                }
                
                server_response.set_body(response);
            }
            else
            {
                stringstream s;
                PLOGE << "Bad Query :" << db.get_query();
                s << "Erreur avec la requête: " << db.get_query();
                server_response.set_body(json::value::string(s.str()));
            }
        }
        else
        {
            server_response.set_body(json::value::string("Erreur de connection à la base"));
        }
        request.reply(server_response);
    }
    else
    {
        request.reply(web::http::status_codes::NotFound);
    }
}

string make_http(string ip, int port)
{
    stringstream s;
    s << "http://" << ip << ":" << port <<"/api/data";
    return s.str();
}