#include "db_models.hpp"


string build_query_select(const Db_template& db_obj,vector<string> field_selection, const char* condition, int limit)
{
    stringstream ss;
    ss << "SELECT ";
    int i=0;
    vector<string> fields = field_selection.empty() ? db_obj.get_keyProperties() : field_selection;
    int last_i = fields.size() -1;
    for(string key: fields)
    {
        ss << (field_selection.empty() ? to_safe_field(key) : key) ;
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    last_i = db_obj.get_properties().size() - 1;
    i=0;
    for(string key: db_obj.get_properties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << " FROM " << to_safe_field(db_obj.get_name());

    if(condition != NULL)
    {
        ss << " WHERE " << condition;
    }
    if(limit > 0)
    {
        ss << " LIMIT " << limit;
    }
    ss << ";";

    return ss.str();
}

string build_query_insert(const Db_template& db_obj,vector<string> field_selection)
{
    stringstream ss;
    ss << "INSERT OR IGNORE INTO " << to_safe_field(db_obj.get_name()) << "(";
    int i=0;
    int last_i = db_obj.get_keyProperties().size() -1;
    for(string key: db_obj.get_keyProperties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    last_i = db_obj.get_properties().size() -1;
    i = 0;
    for(string key: db_obj.get_properties())
    {
        ss << to_safe_field(key);
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << ") VALUES(";
    i=0;
    last_i = db_obj.get_keyProperties().size() -1;
    for(string key: db_obj.get_keyProperties())
    {
        ss << "?";
        if(i++ != last_i || db_obj.get_properties().size()>0)
        {
            ss << ", ";
        }
    }
    i=0;
    vector<string> fields = field_selection.empty() ? db_obj.get_properties() : field_selection;
    last_i = fields.size() -1;
    for(string prop : fields)
    {
        ss << "?";
        if(i++ != last_i)
        {
            ss << ", ";
        }
    }
    ss << ");";
    return ss.str();
}

string build_query_delete(const Db_template& db_obj, const char* condition)
{
    stringstream ss;
    ss << "DELETE FROM " << to_safe_field(db_obj.get_name());
    if(condition != NULL) ss << " WHERE " << condition;
    ss << ";";
    return ss.str();
}

string build_query_update(const Db_template& db_obj,vector<string> field_selection, const char* condition)
{
    stringstream ss;
    ss << "UPDATE " << to_safe_field(db_obj.get_name()) << " SET ";
    int i=0;
    vector<string> fields = field_selection.empty() ? db_obj.get_properties() : field_selection;
    int last_i = fields.size() -1;
    for(string prop : fields)
    {
        ss << to_safe_field(prop) << " = ?";
        if(i++ != last_i)
        {
            ss << ", ";
        }
        
    }
    if(condition != NULL)
    {
        ss << " WHERE " << condition;
    }
    ss << ";";
    return ss.str();
}

string to_safe_field(string s)
{
    stringstream ss;
    ss << "[" << s << "]";
    return ss.str();
}
