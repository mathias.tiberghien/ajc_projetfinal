# Difficultés rencontrées lors du projet

## Application balise

- La documentation des composants éléctroniques n'est pas simple à appréhender
- Beaucoup d'exemples de code amateur
- Les opérations de lecture des registres demandent beaucoup de sous-opérations et de calcul annexes
- Les composants ne sont pas toujours ceux indiqués (exemple du GY-271 censé être un HMC883L et est en fait un QMC883L)
- Problème d'alimentation sur le contrôleur PCA
- Transmission TX/RX: Résultats incompréhensibles lorsque le récepteur utilisent tout les deux des sleeps dans la boucle d'acquisition