# Projet Final Formation AJC Developpeur C/C++ Embarqué

## Description du projet

Ce projet est constitué de 3 sous-projets:
- balise: Implémente une application émettrice de données issues de 3 capteurs en utilisant le protocole TX/RX
- master: Implémente une application réceptrice des données de la balise en TX/RX et qui:
    - Met à jour létat de servo-moteurs en fonction de données reçues
    - Formatte les données reçues en JSON
    - Emet les données JSON en TCP
- serveur: Implémente une application cliente du master en TCP et qui stockera les informations reçues et doit permettre à un utilisateur de :
    - Suivre les informations d'un capteur en temps réel
    - Afficher les données d'une capteur
    - etc

## Architecture Projet

![Schema de flux](./doc/images/architecture_projet.png)

## Connectivite Balise/Maître

![Schema de connexion balise/maître](./doc/images/connectique_maitre_balise.png)

## Sites de références

Raspberry PI 3 wiring: https://pinout.xyz/
Librairies C++ d'accès aux périphériques: 

- Article de référence: https://medium.com/geekculture/raspberry-pi-c-libraries-for-working-with-i2c-spi-and-uart-4677f401b584

- http://wiringpi.com/ (Générique)
- https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/about/?h=v1.6.x (Générique)
- https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/ (UART)
- https://www.kernel.org/doc/Documentation/i2c/smbus-protocol (I2C)
- https://raspberrytips.fr/tutoriel-gpio-raspberry-pi/ (GPIO)
- https://lloydrochester.com/post/hardware/libgpiod-input-rpi/ (libgpiod)

### Ressource d'implémentation du Capteur BME280

Une implémentation en C++, sans dépendances (implémentation perso du protocole I2C)

https://github.com/PhilippeSimier/Capteurs_I2C/tree/master/BME280

### Ressource d'implémentation du Capteur GY-271

Une implémentation en C++, basée sur QT, mais qui contient tous les renseignements d'adressage pour notre capteur

https://github.com/manfredipist/QI2CProtocol
