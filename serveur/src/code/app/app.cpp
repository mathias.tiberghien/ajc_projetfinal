#include "app.hpp"

void set_default_config(json& config)
{
    config["port"] = 5000;
    config["db_path"] = "./balise.db";
    config["db_script"] = "./db_script.sql";
}

void app::loadConfig(json &config)
{
    set_default_config(config);
    std::ifstream f(app::config_filename);
    if (f.fail())
    {
        PLOGD << "Fichier de configuration introuvable : utilisation des valeurs par défaut";
    }
    else
    {
        PLOGI << "Ouverture du fichier de configuration";
        json config_patch = json::parse(f, nullptr, false);
        if (config.is_discarded())
        {
            PLOGW << "Erreur lors de la lecture du fichier de configuration: utilisation des valeurs par défaut";
        }
        else
        {
            config.merge_patch(config_patch);
        }
    }
    PLOGD << "Le fichier de base de données est " << config["db_path"];
}

bool app::init(int argc, char* argv[])
{
    try
    {
        bool init=true;
        init_logs(argc >1 ? string(argv[1]): string(""));
        PLOGI << "Ce programme collecte les données du master et les enregistre en base de données";
        json config;
        this->loadConfig(config);
        init = this->TCP_serv.init(config) && this->dataHandler.init(config);
 
        return init;
    }
    catch(const std::exception& e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
    
    
}

void app::run()
{
    PLOGW << "Lancement du programme";
    try
    {
        
        this->startReceiveLoop();
        char saisie;
        while(true)
        {
            cout << "Tapez q pour quitter:" << endl;
            cin >> saisie;
            if(saisie == 'q')
            {
                break;
            }
        }
        this->stopReceiveLoop();      
    }
    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }
    
    this->exit();
}

void app::startReceiveLoop()
{
    PLOGW << "Démarrage du service de réception des messages";
    pthread_create (&thread, NULL, receiveAndHandleLoop, this);
}

void app::stopReceiveLoop()
{
    pthread_cancel(thread);
    void *result;
    pthread_join(thread, &result);
    PLOGW << "Arrêt du service de réception des messages.";
}

void* receiveAndHandleLoop(void* arg)
{
    app* this_app = (app*)arg;

     // activer les signaux d'annulation
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    // annuler le thread de manière asynchrone
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    //version test, on se connecte directement
    if (this_app->TCP_serv.waitForClient_toConnect())
    {
        while(true)
        {
            if(this_app->TCP_serv.recvSocket())
            {
                string donnee_recue=this_app->TCP_serv.getMessage();
                if(donnee_recue.size() >0)
                {
                    PLOGD << "Reçu du master:" << donnee_recue;
                    if(donnee_recue == "exit")
                    {
                        this_app->TCP_serv.closeClient();
                        bool client_connected = this_app->TCP_serv.waitForClient_toConnect();
                        if(!client_connected)
                        {
                            break;
                        }
                    }
                    else
                    {
                        json data = json::parse(donnee_recue, nullptr, false);
                        if(data.is_discarded())
                        {
                            PLOGF << "Données reçue invalides pour le stockage";
                        }
                        else
                        {
                            this_app->dataHandler.storeData(data);
                        }
                        
                    }
                }
            }
        }
        
    }

    return 0;
}

void app::clean()
{
    PLOGD << "Clean du programme";
    TCP_serv.closeClient();
    TCP_serv.closeServeur();
    this->dataHandler.clean();
}

void app::exit()
{
    PLOGW << "Sortie  du programme";
}