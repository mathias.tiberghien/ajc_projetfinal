#include "dataHandler.hpp"


void DataHandler::storeData(json& json)
{
    Json_Db_Template db_data("data", {"capteur", "nom", "time_stamp"}, json);
    if(this->dbo.init_query(Query_Type::INSERT, db_data))
    {
        bool insertOk = this->dbo.exec_insert(db_data, true);
        if(insertOk)
        {
            PLOGD << "Insertion dans la base de: " << json.dump();
        }
        else
        {
            PLOGE << "Erreur lors de l'insertion de données de: " << json.dump();
        }
    }
    else
    {
        PLOGF << "Erreur lors de l'initialisation de la requête";
    }
}

bool DataHandler::init(const json& config)
{
    this->db_file = config["db_path"];
    this->db_script_file = config["db_script"];
    ifstream db_file(this->db_file);
    ifstream db_script(this->db_script_file);
    if(!db_file.good())
    {
        if(db_script.good())
        {
            PLOGD << "La base de données n'existe pas: création à l'aide du script";
            stringstream buffer;
            buffer << db_script.rdbuf();
            if(this->dbo.connect(this->db_file))
            {
                bool 
                result = this->dbo.exec_custom_query_no_return(buffer.str());
                if(!result)
                {
                    PLOGF << "Echec de la création de la base de données";
                }
                log_init("DataHandler", result);
                return result;
            }
        }
        else
        {
            PLOGF << "Script de création de la base de données introuvable";
        }
        log_init("DataHandler", false);
        return false;
    }
    bool result = this->dbo.connect(this->db_file);
    log_init("DataHandler", result);
    return result;
}

void DataHandler::clean()
{
    this->dbo.disconnect();
}