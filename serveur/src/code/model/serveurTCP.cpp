#include "serveurTCP.hpp"


ServeurTCP::ServeurTCP(int nvPort) : port(nvPort), socketDesc_serv(-1), socketDesc_toClient(-1)
{}


bool ServeurTCP::init(const json& config)
{
    this->port = config["port"];
    bzero((char*)&servAddr, sizeof(servAddr));
    bool initOk = true;
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(this->port);

    PLOGD << "Ouverture sur le port " << this->port; 

    socketDesc_serv = socket(AF_INET, SOCK_STREAM, 0);
    if(socketDesc_serv < 0)
    {
        PLOGF << "Error establishing the server socket" << endl;
        initOk = false;
    }
    if(initOk)
    {
        int bindStatus = bind(socketDesc_serv, (struct sockaddr*) &servAddr, sizeof(servAddr));
        if(bindStatus < 0)
        {
            PLOGF << "Error binding socket to local address" << endl;
            initOk = false;
        }
    }

    log_init("Serveur TCP", initOk);

    return initOk;

}


bool ServeurTCP::waitForClient_toConnect()
{
    PLOGI << "En attente de connection du raspberry master";
    listen(socketDesc_serv, 5);
    socklen_t newSockAddrSize = sizeof(toClientAddr);
    socketDesc_toClient = accept(socketDesc_serv, (sockaddr *)&toClientAddr, &newSockAddrSize);

    if(socketDesc_toClient < 0)
    {
        PLOGI << "Erreur lors de l'acceptation de la requete du client!";
        return false;
    }
    PLOGI << "connection client réussie";


    return true;
}

void ServeurTCP::closeServeur()
{
    PLOGD << "Fermeture serveur avec socket: " << socketDesc_serv;
    if(socketDesc_serv>=0)
    {
        close(socketDesc_serv);
    }
    socketDesc_serv = -1;
}

void ServeurTCP::closeClient()
{
    PLOGD << "Fermeture client avec socket: " << socketDesc_toClient;
    if(socketDesc_toClient >=0)
    {
        close(socketDesc_toClient);
    }
    socketDesc_toClient = -1;
}


bool ServeurTCP::recvSocket()
{
    memset(&message, 0, TAILLE_BUFFER); //clear the buffer
    int nbOctet=recv(socketDesc_toClient, &message, TAILLE_BUFFER, 0);

    if(nbOctet<=0)
    {
        return false;
    }
    if(nbOctet>TAILLE_BUFFER)
    {
        PLOGI << "Reception incomplete du message: taille maximale: " << TAILLE_BUFFER << ", taille reçue: " << nbOctet;
        return false;
    }

    return true;
}

string ServeurTCP::getMessage()
{
    return string(message);
}