#include "json_db_model.hpp"

Json_Db_Template::Json_Db_Template(string name, vector<string> keys, json data, vector<string> ignored):data(data)
{
    this->set_name(name);
    for(auto p: data.items())
    {
        string name = p.key();
        bool is_primitive = p.value().is_primitive();
        
        if(is_primitive)
        {
            bool is_key = is_primitive && find(keys.begin(), keys.end(), name)!=keys.end();
            bool is_ignored = is_primitive && find(ignored.begin(), ignored.end(), name)!=ignored.end();
            if(is_key)
            {
                this->add_keyProperty(name);
            }
            else if(!is_ignored)
            {
                this->add_property(name);
            }
            if(!is_ignored)
            {
                this->is_column_number[name] = p.value().is_number();
            }
            
        }
    }
}

void Json_Db_Template::read_from_stmt(sqlite3_stmt* stmt, vector<string> columns)
{
   for(unsigned int i=0;i<columns.size();i++)
   {
        string key = columns[i];
        if(this->is_column_number[key])
        {
            this->data[key] = sqlite3_column_double(stmt,i);
        }
        else
        {
            this->data[key] = string(get_string_or_empty(sqlite3_column_text(stmt, i)));
        }
        
   }
}

char* string_to_char_array(string value)
{
    unsigned int length = value.size();
    // declaring character array (+1 for null terminator)
    char* char_array = new char[length + 1];
 
    // copying the contents of the
    // string to char array
    strcpy(char_array, value.c_str());
    
    return char_array;
}
        
void Json_Db_Template::bind_to_stmt(sqlite3_stmt* stmt,vector<string> column_selection, bool is_update)
{
    int i=1;
    for(auto k: this->get_keyProperties())
    {
        if(is_update)
        {
            if(this->is_column_number[k])
            {
                sqlite3_bind_double(stmt, i, this->data[k]);
            }
            else
            {
                char * value  =  string_to_char_array(this->data[k]);
                sqlite3_bind_text(stmt, i, value, -1, 0);
                this->binding_values.push_back(value);
            }
        }
        else
        {
            sqlite3_bind_null(stmt, i);
        }
        i++;
    }
    vector<string> fields = column_selection.empty() ? this->get_properties() : column_selection;
    for(auto p: fields)
    {
        if(this->is_column_number[p])
        {
            sqlite3_bind_double(stmt, i, this->data[p]);
        }
        else
        {
            char* value = string_to_char_array(this->data[p]);
            sqlite3_bind_text(stmt, i, value, -1, 0);
            this->binding_values.push_back(value);
        }
        i++;
    }
}

void Json_Db_Template::clean_model()
{
    for(char* v: this->binding_values)
    {
        free(v);
    }
    this->binding_values.clear();
}

string Json_Db_Template::to_string()
{
    return this->data.dump(4);
}

string get_string_or_empty(const unsigned char* text)
{
    const char* t = (const char*)text;
    return t == NULL ? string("") : string(t);
}