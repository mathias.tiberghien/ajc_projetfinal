#include "up11.h"
#include "dataHandler.hpp"
#include "json.hpp"
#include <cstdio>
using json = nlohmann::json_abi_v3_11_2::json; 

UP_SUITE_BEGIN(dataHandler);

const string test_file_db = "./test.db";
const string db_script_file_path = "../../config/db_script.sql";
const string wrong_db_script_file_path = "./test.sql";
DataHandler test_dataHandler;

UP_TEST(dbNoExistScriptNoExist)
{
    remove(test_file_db.c_str());
    json config;
    config["db_path"]= test_file_db;
    config["db_script"]= wrong_db_script_file_path;
    UP_ASSERT(!test_dataHandler.init(config));
}

UP_TEST(dbNoExistScriptExist)
{   
    remove(test_file_db.c_str());
    json config;
    config["db_path"]= test_file_db;
    config["db_script"]= db_script_file_path;
    UP_ASSERT(test_dataHandler.init(config));
}

UP_TEST(dbExist)
{
    json config;
    remove(test_file_db.c_str());
    config["db_path"]= test_file_db;
    config["db_script"]= db_script_file_path;
    test_dataHandler.init(config);
    UP_ASSERT(test_dataHandler.init(config));
}


UP_SUITE_END();