#pragma once

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include "json.hpp"
#include "log.hpp"

using json = nlohmann::json_abi_v3_11_2::json; 

#define TAILLE_BUFFER 2000


/**
 * @brief Server TCP
 * 
 */
class ServeurTCP
{
    public :
    /**
     * @brief Constructeur de la classe
     * 
     * @param nvPort Port d'écoute
     */
    ServeurTCP(int nvPort);
    /**
     * @brief Destructeur de la classe
     * 
     */
    ~ServeurTCP() {}
    /**
     * @brief Initialise le serveur à l'aide d'un fichier de configuration JSON
     * 
     * @param config La configuration JSON
     * @return true si l'initialisation s'est déroulée correctement
     * @return false sinon
     */
    bool init(const json& config);
    /**
     * @brief Attente d'une connection d'un client au serveur TCP
     * 
     * @return true si une connection a réussie
     * @return false sinon
     */
    bool waitForClient_toConnect();
    /**
     * @brief Se met à l'écoute de la réception d'un message TCP
     * 
     * @return true si le message a été reçu
     * @return false sinon
     */
    bool recvSocket();
    /**
     * @brief Fermeture du serveur
     * 
     */
    void closeServeur();
    /**
     * @brief Fermeture du client
     * 
     */
    void closeClient();
    /**
     * @brief Récupère le message obtenu par le serveur
     * 
     * @return string 
     */
    string getMessage();


    private:

    int port;

    //la lib utilise des char* et non des strings
    char message[TAILLE_BUFFER];

    sockaddr_in servAddr;
    sockaddr_in toClientAddr;

    int socketDesc_serv;
    int socketDesc_toClient;
};