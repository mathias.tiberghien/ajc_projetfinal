#pragma once

#include "json.hpp"
#include "db_models.hpp"
#include <list>

using json = nlohmann::json_abi_v3_11_2::json;

/**
 * @brief Implémentation d'un objet JSON avec l'interface SQLITE3
 * 
 */
class Json_Db_Template: public Db_template
{
    public:
        /**
         * @brief Constructeur de la classe
         * 
         * @param name Nom de la table SQLITE3
         * @param keys Liste des clés primaires
         * @param data Elément JSON représentant tous les champs de la table
         * @param ignored Eléments de l'objet JSON à ignorer dans SQLITE3
         */
        Json_Db_Template(string name, vector<string> keys, json data, vector<string> ignored={});
        /**
         * @brief Implémentation de la lecture d'un statement SQLITE3 dans l'objet JSON
         * 
         * @param stmt Le statement SQLITE3
         * @param columns La liste des colonnes de la requête SELECT
         */
        virtual void read_from_stmt(sqlite3_stmt* stmt, vector<string> columns) override;
        /**
         * @brief Implémentation de l'écriture d'un statement SQLITE3 par un objet JSON
         * 
         * @param stmt Le statement SQLITE3
         * @param column_selection La liste des colonnes sélectionnées pour l'INSERT ou l'UPDATE
         * @param is_update Renseigne si la requête est de type INSERT (false) ou UPDATE (true)
         */
        virtual void bind_to_stmt(sqlite3_stmt* stmt, vector<string> column_selection, bool is_update) override;
        /**
         * @brief Nettoye l'objet JSON
         * 
         */
        virtual void clean_model() override;
        /**
         * @brief Renvoit l'objet JSON sous forme de chaîne de caractères
         * 
         * @return string 
         */
        virtual string to_string() override;
        /**
         * @brief Renvoit une référence vers l'objet JSON
         * 
         * @return json& 
         */
        json& getData(){return this->data;};
    private:
        json data;
        map<string,bool> is_column_number;
        vector<char*> binding_values;
};

extern string get_string_or_empty(const unsigned char* text);