#ifndef DB_MODELS_HPP_INCLUDED
#define DB_MODELS_HPP_INCLUDED


#include <iostream>
#include <vector>
#include <sstream>
#include <sqlite3.h>

using namespace std;

/**
 * @brief Modèle d'échange avec l'interface de connection SQLITE
 * 
 */
class Db_template
{
    public:
        /**
         * @brief Constructeur de la classe
         * 
         */
        Db_template(){}
        /**
         * @brief Déstructeur de la classe
         * 
         */
        ~Db_template(){};
        /**
         * @brief Retourne les champs de la table SQLITE
         * 
         * @return vector<string> liste des propriétés de la table SQLITE
         */
        vector<string> get_properties() const {return db_properties;};
        /**
         * @brief Renvoie les champs qui sont des clés primaires dans la table SQLITE
         * 
         * @return vector<string> liste des clés primaires
         */
        vector<string> get_keyProperties() const {return db_keyProperties;};
        /**
         * @brief Nom de la table SQLITE
         * 
         * @return string 
         */
        string get_name() const {return db_name;}
        /**
         * @brief Permet d'implémenter la lecture d'un statement SQLITE3 dans l'objet d'échange
         * 
         * @param stmt Le statement SQLITE3
         * @param columns La liste des columns de la commande SELECT, INSERT ou UPDATE
         */
        virtual void read_from_stmt(sqlite3_stmt* stmt, vector<string> columns)=0;
        /**
         * @brief Permeet de binder les propriétés de l'objet d'échange avec un statement SQLITE3
         * 
         * @param stmt Le statetement SQLITE3 utilisé dans la requête
         * @param column_selection  La sélection de colonnes utilisées dans la requête
         * @param is_update Renseigne si la requête est de type INSERT (false) ou UPDATE (true)
         */
        virtual void bind_to_stmt(sqlite3_stmt* stmt, vector<string> column_selection, bool is_update)=0;
        /**
         * @brief Nettoye l'objet d'échange
         * 
         */
        virtual void clean_model() = 0;
        /**
         * @brief Renvoit l'objet d'échange sous forme de chaîne de caractères
         * 
         * @return string 
         */
        virtual string to_string() = 0;
    protected:
        void set_name(string name){db_name = name;}
        void add_property(string p){db_properties.push_back(p);}
        void add_keyProperty(string p){db_keyProperties.push_back(p);}
    private:
        string db_name;
        vector<string> db_properties;
        vector<string> db_keyProperties;
};



string to_safe_field(string s);
string build_query_select(const Db_template& db_obj,vector<string> field_selection, const char* condition);
string build_query_insert(const Db_template& db_obj,vector<string> field_selection);
string build_query_delete(const Db_template& db_obj, const char* condition);
string build_query_update(const Db_template& db_obj,vector<string> field_selection, const char* condition);

#endif // DB_MODELS_HPP_INCLUDED
