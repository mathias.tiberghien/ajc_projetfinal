#ifndef SQLITE_IO_HPP_INCLUDED
#define SQLITE_IO_HPP_INCLUDED

#include "db_models.hpp"

/**
 * @brief Représente le type de requête effectuée
 * 
 */
typedef enum Query_Type{SELECT, INSERT, UPDATE, DELETE} Query_Type;

/**
 * @brief Classe d'interface avec l' API SQLITE3
 * 
 */
class Db
{
    public:
    /**
     * @brief Constructeur de la classe
     * 
     */
    Db();
    /**
     * @brief Destructeur de la classe
     * 
     */
    ~Db();
    /**
     * @brief Effectue une connection à la base de données
     * 
     * @param path Chemin de la base à ouvrir
     * @return true si la connectione a réussi
     * @return false sinon
     */
    bool connect(string path);
    /**
     * @brief Initialisation de la requête par SQLITE3
     * 
     * @param type Le type de requête
     * @param o Le modèle servant à construire la requête
     * @param field_selection Restriction optionnelle de la sélection de champs
     * @param condition Ajout d'une condition à la requête (en SQL)
     * @return true si l'initialisation s'est déroulée correctement
     * @return false sinon
     */
    bool init_query(Query_Type type, Db_template& o,vector<string> field_selection = {}, const char* condition = NULL);
    /**
     * @brief Permet d'initialiser une transaction
     * 
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool begin_transaction();
    /**
     * @brief Permet de valider la transaction
     * 
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool commit_transaction();
    /**
     * @brief Permet d'effectuer un rollback de la transaction
     * 
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool rollback_transaction();
    /**
     * @brief Récupère la prochaine instance d'une requête SELECT
     * 
     * @param o Modèle permettant de stocker l'instance sélectionnée
     * @return true si une instance a été trouvée
     * @return false sinon
     */
    bool get_next(Db_template& o);
    /**
     * @brief Permet d'exécuter une requête SQLITE en SQL qui ne retourne pas de valeur
     * 
     * @param query Requête SQLITE
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool exec_custom_query_no_return(string query);
    /**
     * @brief Exécute une commande INSERT dans la base
     * 
     * @param o Modèle utilisé pour binder les paramètres
     * @param useIds Définit si les ids du modèles sont utilisés (pas d'auto-incrément)
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool exec_insert(Db_template& o, bool useIds=false);
    /**
     * @brief Exécute une requête de type DELETE
     * 
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool exec_delete();
    /**
     * @brief Exécute une requête de type UPDATE
     * 
     * @param o Modèle utilisé pour binder les paramètres
     * @return true si la commande s'est effectuée correctement
     * @return false sinon
     */
    bool exec_update(Db_template& o);
    /**
     * @brief Retourne le template de la requête exécutée générée par l'interface
     * 
     * @return string la requête SQL
     */
    string get_query(){return query;};
    /**
     * @brief Finalise un objet de requête SQLITE3
     * 
     */
    void finalize_query();
    /**
     * @brief Effectue la déconnexion à la base
     * 
     */
    void disconnect();
    /**
     * @brief Récupère la listes des colonnes utilisées pour la requête
     * 
     * @return vector<string> Liste des colonnes
     */
    vector<string> get_query_columns(){return this->select_columns;}
    private:
        /**
         * @brief Exécution d'une requête dans SQLITE3
         * 
         * @param query la requête
         * @return true si la commande s'est effectuée correctement
         * @return false sino
         */
        bool execute_query(const char* query);
        /**
         * @brief Chemin de la base de données
         * 
         */
        string path;
        /**
         * @brief Interface avec l'api SQLITE3
         * 
         */
        sqlite3* dbo;
        /**
         * @brief Interface avec un statement SQLITE3
         * 
         */
        sqlite3_stmt* stmt;
        /**
         * @brief Type de requête
         * 
         */
        Query_Type query_type;
        /**
         * @brief Colonnes utilisées pour la requête SELECT
         * 
         */
        vector<string> select_columns;
        /**
         * @brief Requête générée par la méthode init_query
         * 
         */
        string query;
};

#endif // SQLITE_IO_HPP_INCLUDED
