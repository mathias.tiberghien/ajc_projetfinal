#pragma once

#include "common.hpp"
#include "json.hpp"
#include "sqlite_io.hpp"
#include "json_db_model.hpp"
#include <fstream>
#include "log.hpp"

using json = nlohmann::json_abi_v3_11_2::json; 

/**
 * @brief Gestionnaire de sauvegarde des données dans la base SQLITE
 * 
 */
class DataHandler
{
    public:
        /**
         * @brief Constructeur de la classe initialisant les paramètres par défaut
         * 
         */
        DataHandler():dbo(Db()), db_file("./balise.db"), db_script_file("./db_script.sql"){};
        /**
         * @brief Initialisation des paramètres à l'aide d'un fichier de configuration
         * 
         * @param config Objet json représentant la configuration de l'application
         * @return true si le DataHandler est initialisé correctement
         * @return false sinon
         */
        bool init(const json& config);
        /**
         * @brief Enregistre la donnée json dans la base SQLITE
         * 
         * @param data 
         */
        void storeData(json& data);
        /**
         * @brief Nettoyage des ressources
         * 
         */
        void clean();
    private:
        /**
         * @brief Interface avec la base de données SQLITE
         * 
         */
        Db dbo;
        /**
         * @brief Chemin de la base de données
         * 
         */
        string db_file;
        /**
         * @brief Chemin du script de création de la base de données
         * 
         */
        string db_script_file;
};