#pragma once

#include <unistd.h>
#include "common.hpp"
#include "log.hpp"
#include <pthread.h>
#include <unistd.h>
#include "serveurTCP.hpp"
#include "dataHandler.hpp"
#include "json.hpp"

using json = nlohmann::json_abi_v3_11_2::json;

/**
 * @brief Classe principale, chargée de lancer la boucle d'écoute en TCP et le système de sauvegarde des données
 * 
 */
class app
{
    public:
        /**
         * @brief Constructeur de la classe
         * 
         */
        app(): TCP_serv(5000), config_filename("./app_config.json"){};
        /**
         * @brief Destructeur de la classe
         * 
         */
        ~app(){this->clean();}
        /**
         * @brief Initialisation à partir des arguments de l'applications
         * 
         * @param argc  Nombre d'arguments
         * @param argv  Liste d'arguments
         * @return true si les composants de l'application ont été initialisés correctement
         * @return false autrement
         */
        bool init(int argc, char* argv[]);
        /**
         * @brief Lancement de l'application
         * 
         */
        void run();
    private:
        /**
         * @brief Nettoyage des ressources
         * 
         */
        void clean();
        /**
         * @brief Sortie du programme
         * 
         */
        void exit();
        /**
         * @brief Démarrage de la boucle de réception
         * 
         */
        void startReceiveLoop();
        /**
         * @brief Arrêt du service de réception
         * 
         */
        void stopReceiveLoop();
        /**
         * @brief Charge le fichier de configuration en mémoire
         * 
         * @param config 
         */
        void loadConfig(json& config);
        /**
         * @brief Implémentation de la boucle de réception et traitement des messages
         * 
         * @param arg Est initialisé avec l'instance de l'application
         * @return void* 
         */
        friend void* receiveAndHandleLoop(void* arg);

        /**
         * @brief Serveur TCP
         * 
         */
        ServeurTCP TCP_serv;
        /**
         * @brief Chemin du fichier de configuration
         * 
         */
        const string config_filename;
        /**
         * @brief Thread d'exécution de la boucle de réception
         * 
         */
        pthread_t thread;
        /**
         * @brief Gestionnaire de sauvegarde des données
         * 
         */
        DataHandler dataHandler;
};

extern void* receiveAndHandleLoop(void* arg);