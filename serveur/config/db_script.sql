CREATE TABLE data(
	capteur varchar(50) NOT NULL,
	nom varchar(50) NOT NULL,
	time_stamp date NOT NULL,
	valeur real NOT NULL,
	primary key(capteur, nom, time_stamp));
CREATE INDEX data_ids on data (capteur, nom);
