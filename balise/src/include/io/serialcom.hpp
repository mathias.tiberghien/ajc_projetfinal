#pragma once

#include "common.hpp"
#include <fstream>
#include <iostream>
#include "log.hpp"
#include "wiringSerial.h"
#include "json.hpp"

using json = nlohmann::json_abi_v3_11_2::json; 

/**
 * @brief Cette classe s'occupe de la communication par le protocole Rx/Tx
 * c'est un protocole entre deux appareils, et sur deux cannaux, Chaque appareil ecrit sur un canal
 * qui sera utilisé en lecture par l'autre appareil.
 * pour que la communication soit ouverte, les deux appareils doivent utiliser le même port Rx/Tx (représenté par 
 * un fichier sur linux)
 * et la même fréquence d'envois des données (BaudRate)
 * 
 */
class SerialCom
{
    public:
    /**
     * @brief Constructeur de la classe serial com
     * la classes est instancié mais la communication n'est pas initialisée
     * 
     * @param port port Xr/Tx à utiliser pour la communication
     * @param baudRate BaudRate à utiliser sur le canal, 9600 par defaut.
     */
        SerialCom(string port, int baudRate = 9600):port(port), baudRate(baudRate), fd(-1){};
        /**
         * @brief inititialise la communication suivant le protocole Rx/Tx
         * il est possible de fournir un port et un baudrate avec l'objet Json passé en paramettre
         *
         * 
         * @param config objet json contenant la configuration
         * @return true si l'initialisation s'est bien passée, false sinon
         */
        bool init(const json& config);
        /**
         * @brief fermeture de la communication
         */
        void close();
        /**
         * @brief l'objet se met à l'écoute de nouveaux message sur le canal en entrée
         */
        void wait_for_data();
        /**
         * @brief envoie un message textuel sur le canal de communication en sortie
         * 
         * @param message 
         */
        void send(string message);
        /**
         * @brief Lit le message sur le canal en entrée.
         * Si il n'y a pas de message renvoie une chaine vide.
         * 
         * @return string 
         */
        string receive();
    private:
        string port;
        int baudRate;
        int fd;
};
