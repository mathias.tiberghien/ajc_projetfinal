#pragma once

#include "json.hpp"

using json = nlohmann::json_abi_v3_11_2::json; //Pour une utilisation plus simple.

/**
 * @brief JsonSerializable est une class virtuelle pour les objets que l'on souhaite pouvoir
 * transformer vers ou à partir d'une structure de donnée en format json.
 */
class JsonSerializable
{
    public:
        /**
        * @brief permet de définir les propriétés d'une classe à partir d'un objet json
        * 
        * @param json objet json, doit contenir une descrption des propriétés à modifier
        */
        virtual void from_json(json json) = 0;
        /**
         * @brief renvoie une instance d'objet json decrivant les propriété de l'objet courant.
         * 
         * @return json 
         */
        virtual json to_json() = 0;
};