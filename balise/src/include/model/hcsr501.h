#ifndef HCSR501_H_INCLUDED
#define HCSR501_H_INCLUDED

#include <iostream>
#include <fstream>
#include "capteur.hpp"

/**
 * @brief classe fille de capteur, elle représente le capteur hcsr501 
 * Il s'agit d'un capteur de présence.
 * L'heritage de la classe capteur implique aussi la possibilité de 
 * fournir les mesures en formats json
 * il n'est pas connecté selon le protocole i2c mais directement sur un pin gpio
 */
class hcsr501: public Capteur
{

public:
/**
 * @brief Constructeur du capteur hcsr501
 * Il instancie la classe mais n'initialise pas le capteur pour recevoir les mesures.
 * 
 * @param pin numéro du pin sur lequel est branché le capteur
 */
    hcsr501(int pin);
    ~hcsr501();

    /**
    * @brief initialise le capteur. 
    * Le capteur est pret à prendre des mesures si l'initialisation s'est bien passée
    * 
    * @return true si l'initialisation du capteur s'est bien passée, false sinon
    */
    bool init() override;
    /**
    * @brief renvois l'ensembles des différentes valeurs mesurées par capteur.
    * Ce capteur mesure uniquement la présence ou non d'une personne
    * 
    * @return vector<Capteur_Value> 
    */
    vector<Capteur_Value> getValues() override;
    /**
    * @brief nettoie l'instance du capteur pour le préparer à être supprimé
    */
    void clean() override;
private:
    int obtenirPresence();
    Capteur_Value presence;


};

#endif // HCSR501_H_INCLUDED