#ifndef PIQMC5883L_H
#define PIQMC5883L_H

#include "i2c.h"
#include <cmath>
#include "capteur.hpp"
#include <vector>
#include "log.hpp"
#include "common.hpp"

/* Register numbers */
#define QMC5883L_X_LSB          0x00
#define QMC5883L_X_MSB          0x01
#define QMC5883L_Y_LSB          0x02
#define QMC5883L_Y_MSB          0x03
#define QMC5883L_Z_LSB          0x04
#define QMC5883L_Z_MSB          0x05
#define QMC5883L_STATUS         0x06
#define QMC5883L_TEMP_LSB       0x07
#define QMC5883L_TEMP_MSB       0x08
#define QMC5883L_CONFIG         0x09
#define QMC5883L_CONFIG2        0x0A
#define QMC5883L_RESET          0x0B
#define QMC5883L_RESERVED       0x0C
#define QMC5883L_CHIP_ID        0x0D

/* Values for the STATUS register */
#define QMC5883L_STATUS_DRDY    0x01        //Data ready
#define QMC5883L_STATUS_OVL     0x02        //Overflow flag
#define QMC5883L_STATUS_DOR     0x04        //Data skipped for reading

/* Values for the CONFIG2 register */
#define QMC5883L_CONFIG2_IENB   0x01        //Interrupt Pin Enabling.
#define QMC5883L_CONFIG2_POL    0x40        //Pointer Roll-over.
#define QMC5883L_CONFIG2_SRST   0x80        //Soft Reset.

/* Oversampling values for the CONFIG register */
#define QMC5883L_CONFIG_OS512   0x00
#define QMC5883L_CONFIG_OS256   0x40
#define QMC5883L_CONFIG_OS128   0x80
#define QMC5883L_CONFIG_OS64    0xC0

/* Range values for the CONFIG register */
#define QMC5883L_CONFIG_2GAUSS  0x00
#define QMC5883L_CONFIG_8GAUSS  0x10

/* Rate values for the CONFIG register */
#define QMC5883L_CONFIG_10HZ    0x00
#define QMC5883L_CONFIG_50HZ    0x04
#define QMC5883L_CONFIG_100HZ   0x08
#define QMC5883L_CONFIG_200HZ   0x0C

/* Mode values for the CONFIG register */
#define QMC5883L_CONFIG_STANDBY 0x00
#define QMC5883L_CONFIG_CONT    0x01

#define DECLINATION 3.339

/**
 * @brief classe fille de capteur, elle représente le capteur PiQMC5883L
 * Ce capteur 
 * L'heritage de la classe capteur implique aussi la possibilité de 
 * fournir les mesures en formats json 
 */
class PiQMC5883L: public Capteur
{

    public:
    /**
     * @brief Constructeur du PiQMC5883L
     * Il instancie la classe mais n'initialise pas le capteur pour recevoir les mesures.
     * 
     * @param i2c_bus adresse i2c du capteur PiQMC5883L
     * @param i2c_address bus i2c du capteur PiQMC5883L
     */
        explicit PiQMC5883L(int i2c_bus, int i2c_address);
        ~PiQMC5883L();
        /**
         * @brief indique si le capteur PiQMC5883L est pret à envoyer une mesure
         * 
         * @return true si capteur pret, false sinon
         */
        bool isDataReady();
        
        /**
        * @brief initialise le capteur. 
        * 
        * instancie la représentation i2c interne et calibre la puce à partir de
        * la calilbration standard définie dans cette classe.
        * Le capteur est pret à prendre des mesures si l'initialisation s'est bien passée
        *
        * @return true si l'initialisation du capteur s'est bien passée, false sinon
        */
        bool init() override;
        /**
         * @brief renvois l'ensembles des différentes valeurs mesurées par capteur.
         * le piqmc5883l renvoie son orientation dans l'espace, celle-ci est renvoyée à traver
         * trois Capteur_values différentes, une pour chaque axe (x, y et z)
         * 
         * @return vector<Capteur_Value> 
         */
        vector<Capteur_Value> getValues() override;
        /**
        * @brief nettoie l'instance du capteur pour le préparer à être supprimé
        */
        void clean() override;
    private:
        void softReset();
        void defineSetResetPeriod();
        void disableInterrupt();
        void setConfiguration();

        void setMode(const int &x);
        void setOversampling(const int &x);
        void setRange(const int &x);
        void setSamplingRate(const int &x);

        bool readRaw(int16_t *x, int16_t *y, int16_t *z);

        void setCalibrationOffsets(const int &c1, const int &c2, const int &c3,
                            const int &c4, const int &c5, const int &c6);
        void setCalibrationOffsets(const float &c1, const float &c2, const float &c3,
                            const float  &c4, const float &c5, const float &c6,
                            const float &c7, const float &c8, const float &c9);

        i2c *deviceI2C;

        vector<vector<float>> calibration2;
        vector<vector<int>> calibration;
        float declination = 10.0;

        uint8_t mode;
        uint8_t rate;
        uint8_t range;
        uint8_t oversampling;
        Capteur_Value x;
        Capteur_Value y;
        Capteur_Value z;
        int xlow,ylow,xhigh,yhigh=0;
};

#endif // PIQMC5883L_H
