#pragma once

#include "common.hpp"
#include <vector>
#include <ctime>
#include "jsonSerializable.hpp"

/**
 * @struct Capteur_Value
 * @brief valeur mesurée par un capteur
 * cette structure représente une prise de mesure avec son heure de mesure.
 * 
 */
struct Capteur_Value
{
    /**
     * @brief créer une Capteur_Value à partir d'un objet json
     * 
     * @param json  
     */
        void from_json(json json);
        /**
         * @brief Créer un json 
         * 
         * @return json 
         */
        json to_json();
        /**
         * @brief Nom de la valeur mesurée
         */
        string nom;
        /**
         * @brief heure exacte de la mesure
         */
        time_t time_stamp;
        /**
         * @brief valeur de la mesure 
         */
        double value;
};

/**
 * @brief Class virtuelle qui représente un capteur
 * 
 * Cette classe hérite de jsonSerializable.  
 * @todo paragraphe décrivant la class et le choix de l'héritage
 * 
 */
class Capteur: public JsonSerializable
{
    public:
    /**
     * @brief Construct a new Capteur object
     * 
     * @param nom nom du capteur
     * @param adresse adresse du capteur selon le protocole i2c
     * @param bus bus i2c sur lequel se trouve le capteur (1 par defaut)
     */
        Capteur(string nom, int adresse, int bus=1):
        nom(nom), adresse(adresse), bus(bus){};
        virtual ~Capteur(){};
        /**
         * @brief renvoie le Nom du capteur
         * 
         * @return string 
         */
        string getNom() const {return this->nom;}
        /**
         * @brief définit le Nom du capteur
         * 
         * @param value string
         */
        void setNom(string value){this->nom = value;}
        /**
         * @brief renvois l'adresse i2c du capteur
         * 
         * @return int 
         */
        int getAdresse() const {return this->adresse;}
        /**
         * @brief définit l'adresse i2c du capteur
         * 
         * @param value int
         */
        void setAdresse(int value){this->adresse = value;}
        /**
         * @brief renvois le bus i2c du capteur
         * 
         * @return int 
         */
        int getBus() const {return this->bus;}
        /**
         * @brief définit le bus i2c du capteur
         * 
         * @param value int 
         */
        void setBus(int value){this->bus = value;}
        /**
         * @brief initialise le capteur afin qu'il soit pret à effectuer des mesures
         * Ceci est une fonction virtuelle qui doit être implanter dans les classes filles
         * 
         * @return true si le capteur est bien initialisé, false sinon
         */
        virtual bool init()=0;
        /**
         * @brief renvois l'ensembles des différentes valeurs mesurées par capteur.
         * Pour chaque type de mesure différente, seule la dernière valeur est renvoyée.
         * Ceci est une fonction virtuelle qui doit être implanter dans les classes filles
         * 
         * @return vector<Capteur_Value> 
         */
        virtual vector<Capteur_Value> getValues() =0;
        /**
         * @brief nettoie l'instance du capteur pour le préparer à être supprimé
         * Ceci est une fonction virtuelle qui doit être implanter dans les classes filles
         */
        virtual void clean()=0;
        /**
         * @brief définis les propriétés du capteur à partir d'une instance json. 
         * @param json objet json qui doit définir les propriété "nom", "adresse" et "bus"
         */
        void from_json(json json) override;
        /**
         * @brief renvois une instance json qui contient les propriétés du capteur.
         * 
         * @return json 
         */
        json to_json() override;
    private:
        string nom;
        int adresse;
        int bus;
};