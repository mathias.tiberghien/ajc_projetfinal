//**************************************************************************
/*
    \file     bme280.h
    \author   Philippe SIMIER (Touchard Wahington le Mans)
    \license  BSD (see license.txt)

    \brief    Classe pour le composant i2c  BME280
    \detail   Le BME280 est un capteur environnemental pour mesurer la température,
              la pression barométrique et l'humidité relative! Ce capteur est idéal
              pour réaliser une petite station météo. Il peut être connecté sur
              un bus I2C ou SPI! La broche CSB doit être connecté à VDDIO pour
              sélectionner l'interface I²C. Son adresse sur le bus est 0x77 ou 0x76
              en fonction du niveau de tension appliquée sur la broche SDO.

    \version    2.0 - First release
*/



#ifndef BME280_H_INCLUDED
#define BME280_H_INCLUDED

#include "log.hpp"
#include <iomanip>
#include "i2c.h"
#include <stdint.h>       // pour les types uint8_t etc
#include <math.h>
#include "capteur.hpp"

/**
 * @def adresse par defaut du capteur quand il est alimenté en 5V 
 */
#define ADRESSE_I2C_DEFAUT              0x77
//Register names:

#define DIG_T1        0x88
#define DIG_T2        0x8A
#define DIG_T3        0x8C
#define DIG_P1        0x8E
#define DIG_P2        0x90
#define DIG_P3        0x92
#define DIG_P4        0x94
#define DIG_P5        0x96
#define DIG_P6        0x98
#define DIG_P7        0x9A
#define DIG_P8        0x9C
#define DIG_P9        0x9E
#define DIG_H1        0xA1
#define DIG_H2        0xE1
#define DIG_H3        0xE3
#define DIG_H4        0xE4
#define DIG_H5        0xE5
#define DIG_H6        0xE7
#define CHIPID        0xD0
#define VERSION       0xD1
#define SOFTRESET     0xE0
#define BME280_RESET  0xB6
#define CAL26         0xE1
#define CONTROLHUMID  0xF2
#define CONTROL       0xF4
#define CONFIG        0xF5
#define PRESSUREDATA  0xF7
#define TEMPDATA      0xFA
#define HUMIDDATA     0xFD

#define MEAN_SEA_LEVEL_PRESSURE         1013


/**
 * @brief les registres des données pour la pression température et humidité
 * 
 */
typedef struct
{
  uint8_t pmsb;
  uint8_t plsb;
  uint8_t pxsb;

  uint8_t tmsb;
  uint8_t tlsb;
  uint8_t txsb;

  uint8_t hmsb;
  uint8_t hlsb;

  uint32_t temperature;
  uint32_t pressure;
  uint32_t humidity;

} bme280_raw_data;


/**
 * @brief structure pour enregistrer les constantes de calibration du bme280
 * 
 */
typedef struct
{
  uint16_t dig_T1;
  int16_t  dig_T2;
  int16_t  dig_T3;

  uint16_t dig_P1;
  int16_t  dig_P2;
  int16_t  dig_P3;
  int16_t  dig_P4;
  int16_t  dig_P5;
  int16_t  dig_P6;
  int16_t  dig_P7;
  int16_t  dig_P8;
  int16_t  dig_P9;

  uint8_t  dig_H1;
  int16_t  dig_H2;
  uint8_t  dig_H3;
  int16_t  dig_H4;
  int16_t  dig_H5;
  int8_t   dig_H6;

  int32_t  t_fine;
} bme280_calib_data;

/**
 * @brief classe fille de capteur, elle représente le capteur bme280 
 * L'heritage de la classe capteur implique aussi la possibilité de 
 * fournir les mesures en formats json.
 * on récupère trois données :
 * -la température en °c
 * -la pression en hpa
 * -l'humidité de l'air en % 
 */
class bme280: public Capteur
{

public:
    /**
     * @brief Constructeur du bme280 
     * Il instancie la classe mais n'initialise pas le capteur pour recevoir les mesures.
     * 
     * @param i2cAddress adresse i2c du capteur bme280
     * @param i2cBus bus i2c du capteur bme280
     */
    bme280(int i2cAddress=ADRESSE_I2C_DEFAUT, int i2cBus=I2C_DEFAULT_BUS);
    // le destructeur
    ~bme280();

    /**
     * @brief méthode pour vérifier la présence du composant sur le bus I2C
     * 
     * @return true si il y a une erreur, false sinon
     */
    bool obtenirErreur();

    /**
     * @brief Méthode pour obtenir le Chip ID (0x60 pour le BME280)
     * 
     * @return est sensé retourne 0x60 pour le BME280
     */
    unsigned int obtenirChipID();

    // méthode d'implémentation du Capteur
    /**
     * @brief initialise le capteur. 
     * 
     * instancie la représentation i2c interne et calibre la puce à partir de
     * la calilbration standard définie dans cette classe.
     * Le capteur est pret à prendre des mesures si l'initialisation s'est bien passée
     *
     * @return true si l'initialisation du capteur s'est bien passée, false sinon
     */
    bool init() override;
    /**
    * @brief renvois l'ensembles des différentes valeurs mesurées par capteur.
    * dans le cadre du bme 280, on récupère trois données :
    * -la température en °c
    * -la pression en hpa
    * -l'humidité de l'air en %
    * 
    * @return vector<Capteur_Value> 
    */
    vector<Capteur_Value> getValues() override;
    /**
    * @brief nettoie l'instance du capteur pour le préparer à être supprimé
    */
    void clean() override;
private:


    i2c *deviceI2C;                   // file descriptor
    bme280_calib_data cal;           // calibration
    bme280_raw_data raw;             // les registres
    double h;                         // différence d'altitude du capteur avec le niveau de la mer en m
    bool   error;
    void   readCalibrationData();
    void   getRawData();
    double   obtenirTemperatureEnC();
    double   obtenirPression();
    double   obtenirHumidite();
    Capteur_Value temperature;
    Capteur_Value humidite;
    Capteur_Value pression;
};

#endif // BME280_H_INCLUDED
