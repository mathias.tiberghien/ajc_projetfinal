#include "app.hpp"

using json = nlohmann::json_abi_v3_11_2::json;

bool app::initCapteurs(const json &config)
{
    PLOGD << "Initialisation des capteurs";
    bme280 *c1 = new bme280(config["bme280"]["adresse"], config["bme280"]["bus"]);
    PiQMC5883L *c2 = new PiQMC5883L(config["gy-271"]["bus"], config["gy-271"]["adresse"]);
    hcsr501 *c3 = new hcsr501(config["hc-sr501"]["pin"]);
    this->capteurs.push_back(c1);
    this->capteurs.push_back(c2);
    this->capteurs.push_back(c3);
    bool initOk = true;
    for (auto c : this->capteurs)
    {
        bool initOk = c->init() && initOk;
    }
    return initOk;
}

void set_default_config(json& config)
{
    config["frequence"] = 1;
    config["bme280"]["bus"] = 1;
    config["bme280"]["adresse"] = 0x76;
    config["gy-271"]["bus"] = 1;
    config["gy-271"]["adresse"] = 0x0d;
    config["hc-sr501"]["pin"] = 23;
    config["serial"]["device"] = "/dev/serial0";
    config["serial"]["baud"] = 9600;
}

void app::loadConfig(json &config)
{
    set_default_config(config);
    std::ifstream f(app::config_filename);
    if (f.fail())
    {
        PLOGD << "Fichier de configuration introuvable : utilisation des valeurs par défaut";
    }
    else
    {
        PLOGI << "Ouverture du fichier de configuration";
        json config_patch = json::parse(f, nullptr, false);
        if (config.is_discarded())
        {
            PLOGW << "Erreur lors de la lecture du fichier de configuration: utilisation des valeurs par défaut";
        }
        else
        {
            config.merge_patch(config_patch);
        }
    }
    this->setPeriodeRepos(config["frequence"]);
    int bu_bme = config["bme280"]["bus"];
    PLOGD << "le bus du bme280 est " << bu_bme;
}

void app::setPeriodeRepos(int frequence)
{
    if(frequence>=1)
    {
        this->dureeRepos = frequence;  
    }
    else
    {
        PLOGW << "La fréquence doit être supérieure à 1. Valeur demandée: " << frequence;
    }

}

bool app::initSerial(const json& config)
{
    return serial.init(config);
}

bool app::init(int argc, char *argv[])
{
    PLOGW << "Lancement du programme";
    try
    {
        init_logs(argc > 1 ? string(argv[1]) : string(""));
        PLOGI << "Démarrage de l'application Balise";
        PLOGI << "Le rôle de cette application est de lire les données des capteurs";
        PLOGI << "BME2280 (humidite, température, pression), HC-SR501 (présence) et GY-271 (données magnétiques x,y,z)";
        json config;
        this->loadConfig(config);
        bool initOk = initCapteurs(config);
        return (initOk && initSerial(config));
    }
    catch (const std::exception &e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
}

void app::run()
{
    try
    {
        this->startReadLoop();
        char saisie;
        while (true)
        {
            cout << "Tapez q pour quitter:" << endl;
            cin >> saisie;
            if (saisie == 'q')
            {
                break;
            }
        }
        this->stopReadLoop();
    }
    catch (const exception &e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }
    catch (...)
    {
        PLOGF << "Erreur non gérée, le programme va fermer.";
    }

    this->exit();
}

void app::startReadLoop()
{
    PLOGW << "Démarrage du service de lecture des capteurs";
    pthread_create(&thread, NULL, readAndSendLoop, this);
}

void app::stopReadLoop()
{
    pthread_cancel(thread);
    void *result;
    pthread_join(thread, &result);
    PLOGW << "Arrêt du service de lecture des capteurs.";
}

void *readAndSendLoop(void *arg)
{
    app *this_app = (app *)arg;
    // activer les signaux d'annulation
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

    // annuler le thread de manière asynchrone
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    while (true)
    {
        time_t time_stamp = time(nullptr);
        for (auto capteur : this_app->capteurs)
        {
            vector<Capteur_Value> properties = capteur->getValues();
            string id_capteur = capteur->getNom();
            json prop_json;
            for (auto p : properties)
            {
                p.time_stamp = time_stamp;
                json data = p.to_json();
                data["capteur"] = id_capteur;
                this_app->serial.send(data.dump());
                
            }          
        }
        sleep(this_app->dureeRepos);
    }
        

    return 0;
}

void app::cleanCapteurs()
{
    PLOGD << "Nettoyage des capteurs";
    for (auto c : this->capteurs)
    {
        c->clean();
        delete c;
    }
    this->capteurs.clear();
}

void app::clean()
{
    PLOGD << "Clean du programme";
    this->cleanCapteurs();
    this->serial.close();
}

void app::exit()
{
    PLOGW << "Sortie  du programme";
}