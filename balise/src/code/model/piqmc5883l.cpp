#include "piqmc5883l.h"

bool PiQMC5883L:: init()
{
    deviceI2C = new i2c(this->getAdresse(), this->getBus());
    softReset();
    disableInterrupt();
    defineSetResetPeriod();

    setMode(1);
    setOversampling(512);
    setRange(2);
    setSamplingRate(10);
    setConfiguration();

    //calibrate();
    setCalibrationOffsets(-2817, 4392, -2430, 9775, -4200, 5997);
    //setCalibrationOffsets(1.0000912557105424, 0.003915423209277943, 418.0882138871126, 0.003915423209277943, 1.1679953924705968, -923.1265637992069, 0.0, 0.0, 1.0);

    bool initOk = (deviceI2C->ReadReg8(QMC5883L_CHIP_ID) == 0xFF);
    log_init(this->getNom(), initOk);
    return initOk;     
}

void PiQMC5883L::clean()
{
    if (deviceI2C != NULL)
    {
        PLOGD << "Clean de " << this->getNom();
        delete deviceI2C;
        deviceI2C = nullptr;
    }  
}

vector<Capteur_Value> PiQMC5883L::getValues()
{
    if(this->deviceI2C != nullptr)
    {
        int16_t d_x,d_y,d_z;
        if(readRaw(&d_x,&d_y,&d_z))
        {
            x.value = d_x;
            y.value = d_y;
            z.value = d_z;
        }
        else
        {
            PLOGD << "Echec de la lecture des valeurs de " << this->getNom();
        }

        return {x,y,z};
    }
    throw logic_error("Capteur non initialisé");
}

PiQMC5883L::PiQMC5883L(int i2c_bus, int i2c_address) :
 Capteur("GY-271", i2c_address, i2c_bus),x({.nom = "x"}),
    y({.nom = "y"}), z({.nom = "z"})
{
}

PiQMC5883L::~PiQMC5883L(){
    this->clean();
}

void PiQMC5883L::softReset()
{
    deviceI2C->WriteReg8(QMC5883L_CONFIG2, QMC5883L_CONFIG2_SRST);
}

void PiQMC5883L::disableInterrupt(){
    deviceI2C->WriteReg8(QMC5883L_CONFIG2, QMC5883L_CONFIG2_IENB);
}

void PiQMC5883L::defineSetResetPeriod()
{
    deviceI2C->WriteReg8(QMC5883L_RESET, 0x01);
}

void PiQMC5883L::setConfiguration()
{
    deviceI2C->WriteReg8(QMC5883L_CONFIG, mode|oversampling|range|rate);
}

void PiQMC5883L::setMode(const int &x){
    switch(x) {
        case 0:
            mode = QMC5883L_CONFIG_STANDBY;
        break;
        case 1:
            mode = QMC5883L_CONFIG_CONT;
        break;
    }
}

void PiQMC5883L::setOversampling(const int &x)
{
    switch(x) {
        case 512:
            oversampling = QMC5883L_CONFIG_OS512;
            break;
        case 256:
            oversampling = QMC5883L_CONFIG_OS256;
            break;
        case 128:
            oversampling = QMC5883L_CONFIG_OS128;
            break;
        case 64:
            oversampling = QMC5883L_CONFIG_OS64;
            break;
    }
}

void PiQMC5883L::setRange(const int &x)
{
    switch(x) {
        case 2:
            range = QMC5883L_CONFIG_2GAUSS;
        break;
        case 8:
            range = QMC5883L_CONFIG_8GAUSS;
        break;
    }
}

void PiQMC5883L::setSamplingRate(const int &x)
{
    switch(x) {
        case 10:
            rate = QMC5883L_CONFIG_10HZ;
            break;
        case 50:
            rate = QMC5883L_CONFIG_50HZ;
            break;
        case 100:
            rate = QMC5883L_CONFIG_100HZ;
            break;
        case 200:
            rate = QMC5883L_CONFIG_200HZ;
            break;
    }
}

bool PiQMC5883L::readRaw(int16_t *x, int16_t *y, int16_t *z)
{
    if(!isDataReady())
        return false;

    int data[6];
    deviceI2C->ReadBlockData(QMC5883L_X_LSB, 6, data);
    *x = (data[0] | (data[1] << 8));
    *y = (data[2] | (data[3] << 8));
    *z = (data[4] | (data[5] << 8));

    return true;
}

void PiQMC5883L::setCalibrationOffsets(const int &c1, const int &c2, const int &c3,
                                    const int &c4, const int &c5, const int &c6) {
    vector<int> vec;

    vec.push_back(c1);
    vec.push_back(c2);

    calibration.push_back(vec);
    vec.clear();

    vec.push_back(c3);
    vec.push_back(c4);

    calibration.push_back(vec);
    vec.clear();

    vec.push_back(c5);
    vec.push_back(c6);

    calibration.push_back(vec);
}

void PiQMC5883L::setCalibrationOffsets(const float &c1, const float &c2, const float &c3,
                                           const float &c4, const float &c5, const float &c6,
                                           const float &c7, const float &c8, const float &c9) {
    vector<float> vec;

    vec.push_back(c1);
    vec.push_back(c2);
    vec.push_back(c3);

    calibration2.push_back(vec);
    vec.clear();

    vec.push_back(c4);
    vec.push_back(c5);
    vec.push_back(c6);

    calibration2.push_back(vec);
    vec.clear();

    vec.push_back(c7);
    vec.push_back(c8);
    vec.push_back(c9);

    calibration2.push_back(vec);
}

bool PiQMC5883L::isDataReady()
{
    uint8_t status = deviceI2C->ReadReg8(QMC5883L_STATUS);
    return status & QMC5883L_STATUS_DRDY;
}