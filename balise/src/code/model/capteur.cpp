#include "capteur.hpp"

void Capteur_Value::from_json(json json)
{
    this->nom = json["nom"];
    this->time_stamp = json["time_stamp"];
    this->value = json["valeur"];
}

json Capteur_Value::to_json()
{
    json result;
    result["nom"] = this->nom;
    result["time_stamp"] = this->time_stamp;
    result["valeur"] = this->value;
    return result;
}

void Capteur::from_json(json json)
{
    this->nom = json["nom"];
    this->adresse = json["adresse"];
    this->bus = json["bus"];
}

json Capteur::to_json()
{
    json result;
    result["nom"] = this->nom;
    result["bus"] = this->bus;
    result["adresse"] = this->adresse;
    return result;
}