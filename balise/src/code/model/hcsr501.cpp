#include "log.hpp"

#include "hcsr501.h"

using namespace std;

bool hcsr501:: init()
{
    PLOGD << "Initialisation de " << this->getNom() << " sur le gpio " << this->getAdresse();

    string fileExist = "/sys/class/gpio" + to_string(getAdresse()); 
    ifstream file(fileExist);
    bool initOk = true;
    if(!file.good())
    {
        initOk = false;
        PLOGD << "Export du GPIO " << this->getAdresse();
        ofstream export_file("/sys/class/gpio/export");
        if(export_file.good())
        {
            export_file << getAdresse();
            export_file.close();
            ofstream direction_file("/sys/class/gpio/gpio" + to_string(getAdresse()) + "/direction");
            if(direction_file.good())
            {
                direction_file << "in";
                direction_file.close();
                initOk = true;
            }
        }
    }
    log_init(this->getNom(), initOk);
    return initOk;

}

void hcsr501::clean()
{
}

vector<Capteur_Value> hcsr501:: getValues()
{
    presence.value = obtenirPresence();
    return {presence};
}

hcsr501::hcsr501(int pin):
    Capteur("HCSR501", pin), presence({.nom = "presence"})
{
}

hcsr501::~hcsr501()
{
    this->clean();
}

int hcsr501::obtenirPresence()
{

    ifstream value_file("/sys/class/gpio/gpio" + to_string(getAdresse()) + "/value");
    int value;

    value_file >> value;
    value_file.close();

    return value;
}