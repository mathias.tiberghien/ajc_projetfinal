# Application Balise

## Description de l'application

L'application doit tourner sur une Raspberry Pi 3 Model B+ dotée de 3 capteurs :
- Capteur d'humidité BME280
- Capteur Triple axis-boussole magnétomètre - GY-271 (QMC883L)
- Détecteur de mouvement infrarouge HC-SR501

## Schéma de connection:

Informations de connection

![Connectique de la carte balise](./doc/images/connectique_balise.png)


## Capteur BME280

Permet de mesurer:

- La température (entre -45°C et +85°C)
- La pression (entre 300 hPa et 1100 hPa)
- L'hygrométrie (entre 0 et 100%)


## Capteur GY-271

Permet de mesurer:

- Champs d’induction magnétique (entre -8 à et 8 gauss)
- Données calculées en x, y et z

## Capteur HC-SR501

Permet de mesurer:

- Distance (entre 3 et 7 mètres)
- Direction (plage de détection 140°)